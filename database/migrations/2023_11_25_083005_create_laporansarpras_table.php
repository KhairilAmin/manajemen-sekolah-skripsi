<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporansarprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporansarpras', function (Blueprint $table) {
            $table->id();
            $table->string('id_sarpras');
            $table->string('id_pelapor');
            $table->enum('tingkat_kerusakan', ['ringan', 'sedang', 'berat']);
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporansarpras');
    }
}
