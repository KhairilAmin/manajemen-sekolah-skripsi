<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanSarprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_sarpras', function (Blueprint $table) {
            $table->id();
            $table->string('id_sarpras');
            $table->string('keterangan');
            $table->integer('nominal');
            $table->integer('status');
            $table->string('id_pengaju');
            $table->string('id_penerima')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_sarpras');
    }
}
