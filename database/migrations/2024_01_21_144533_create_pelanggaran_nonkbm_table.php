<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelanggaranNonkbmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggaran_nonkbm', function (Blueprint $table) {
            $table->id();
            $table->string('id_siswa');
            $table->string('id_bk');
            $table->string('id_semester');
            $table->string('poin');
            $table->string('keterangan');
            $table->date('tanggal_pelanggaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggaran_nonkbm');
    }
}
