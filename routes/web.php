<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(['auth']);

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

//Route Walimurid
Route::group(['middleware'=>'role:walimurid'],function () {
    
    //Dashboard
    Route::get('/walimurid/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardwalimurid'])->name('dashboard-walimurid');
    
    //SPP
    Route::get('/walimurid/pembayaran', [App\Http\Controllers\TagihanController::class, 'pembayaransiswa'])->name('pembayaran-siswa');

    //Laporan
    Route::get('/walimurid/laporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexcreatelaporan'])->name('indexcreatelaporan-walimurid');
    Route::get('/walimurid/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'formlaporan'])->name('formlaporan-walimurid');
    Route::post('/walimurid/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('formlaporan-walimurid');
});

//Route Siswa
Route::group(['middleware'=>'role:siswa'],function () {
    //Dashboard
    Route::get('/siswa/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardsiswa'])->name('dashboard-siswa');
    
    //SPP
    Route::get('/siswa/pembayaran', [App\Http\Controllers\TagihanController::class, 'pembayaransiswa'])->name('pembayaran-siswa');

    //Laporan
    Route::get('/siswa/laporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexcreatelaporan'])->name('indexcreatelaporan-siswa');
    Route::get('/siswa/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'formlaporan'])->name('formlaporan-siswa');
    Route::post('/siswa/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('formlaporan-siswa');

    //Nilai
    Route::get('/siswa/nilai', [App\Http\Controllers\NilaiSiswaController::class, 'index'])->name('indexnilai-siswa');
    Route::get('/siswa/nilai/{id}', [App\Http\Controllers\NilaiSiswaController::class, 'indexnilai'])->name('dataindexnilai-siswa');

    Route::get('/siswa/nilaisebelumnya', [App\Http\Controllers\NilaiSiswaController::class, 'indexbefore'])->name('indexnilai-siswa');
    Route::get('/siswa/nilai/{id}', [App\Http\Controllers\NilaiSiswaController::class, 'indexnilai'])->name('dataindexnilai-siswa');
    Route::post('/siswa/nilaisebelumnya', [App\Http\Controllers\NilaiSiswaController::class, 'indexbeforemapel'])->name('indexnilai-siswa');
    Route::get('/siswa/nilaisebelumnya/{id_mapel}/{id_log}', [App\Http\Controllers\NilaiSiswaController::class, 'indexbeforenilai'])->name('dataindexnilai-siswa');

    //Absensi
    Route::get('/siswa/absensi', [App\Http\Controllers\AbsensiController::class, 'siswaindex'])->name('indexabsensi-siswa');
    Route::post('/siswa/absensi', [App\Http\Controllers\AbsensiController::class, 'siswaindexdetail'])->name('indexabsensi-siswa');
    
    //PelanggaranKBM
    Route::get('/siswa/pelanggarankbm', [App\Http\Controllers\PelanggaranKbmController::class, 'siswaindex'])->name('indexpelanggarankbm-siswa');
    
    //Siswa
    Route::get('/siswa/rapor', [App\Http\Controllers\RaporController::class, 'indexsiswa'])->name('indexraporsiswa');
    Route::post('/siswa/rapor', [App\Http\Controllers\RaporController::class, 'indexsiswadetail'])->name('indexdetailraporsiswa');


});

//Route Guru
Route::group(['middleware'=>'role:guru'],function () {
    Route::get('/guru/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardguru'])->name('dashboard-guru');
    
    //Komponen Nilai
    Route::get('/guru/komponennilai', [App\Http\Controllers\GuruController::class, 'indexkomponen'])->name('indexkomponen-guru');
    Route::get('/guru/komponennilai/{id}', [App\Http\Controllers\GuruController::class, 'datakomponen'])->name('indexkomponen-guru');
    Route::get('/guru/komponennilai/{id}/add', [App\Http\Controllers\GuruController::class, 'formaddkomponen'])->name('fromaddkomponen-guru');
    Route::post('/guru/komponennilai/{id}/add', [App\Http\Controllers\GuruController::class, 'actoinaddkomponen'])->name('actionaddkomponen-guru');

    //Laporan
    Route::get('/guru/laporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexcreatelaporan'])->name('indexcreatelaporan-guru');
    Route::get('/guru/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'formlaporan'])->name('formlaporan-guru');
    Route::post('/guru/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('formlaporan-guru');

    //Nilai
    Route::get('/guru/inputnilai', [App\Http\Controllers\GuruController::class, 'indexinputnilai'])->name('indexinputnilai-guru');
    Route::get('/guru/inputnilai/{id}', [App\Http\Controllers\GuruController::class, 'komponeninputnilai'])->name('komponeninputnilai-guru');
    Route::get('/guru/inputnilai/{id}/input', [App\Http\Controllers\GuruController::class, 'inputnilaisiswa'])->name('inputnilaisiswa-guru');
    Route::post('/guru/inputnilai/{id}/input', [App\Http\Controllers\GuruController::class, 'actioninputnilaisiswa'])->name('inputnilaisiswa-guru');

    //GenerateRapor
    Route::get('/guru/generaterapor', [App\Http\Controllers\RaporController::class, 'indexguru'])->name('indexrapor-guru');
    Route::get('/guru/generaterapor/{id}', [App\Http\Controllers\RaporController::class, 'generateguru'])->name('generaterapor-guru');
    Route::get('/guru/rapor', [App\Http\Controllers\RaporController::class, 'seeguru'])->name('indexseerapor-guru');
    Route::get('/guru/rapor/{id}', [App\Http\Controllers\RaporController::class, 'seedetailguru'])->name('seedetailrapor-guru');

    //Absensi
    Route::get('/guru/absensi', [App\Http\Controllers\AbsensiController::class, 'guruindex'])->name('absensi-guru');
    Route::post('/guru/absensi', [App\Http\Controllers\AbsensiController::class, 'guruindexaction'])->name('absensi-guru');
    Route::get('/guru/absensi/{id_tanggal}/{id_kelas}', [App\Http\Controllers\AbsensiController::class, 'isiabsensi'])->name('isiabsensi-guru');
    Route::post('/guru/absensi/{id_tanggal}/{id_kelas}', [App\Http\Controllers\AbsensiController::class, 'submitabsensi'])->name('submitabsensi-guru');

    //PelanggaranKBM
    Route::get('/guru/pelanggarankbm', [App\Http\Controllers\PelanggaranKbmController::class, 'guruindex'])->name('pelanggarankbm-guru');
    Route::get('/guru/pelanggarankbm/{id}', [App\Http\Controllers\PelanggaranKbmController::class, 'forminput'])->name('pelanggarankbm-guru');
    Route::post('/guru/pelanggarankbm/{id}', [App\Http\Controllers\PelanggaranKbmController::class, 'addforminput'])->name('addpelanggarankbm-guru');

    //Walikelas
    Route::get('/guru/walikelaspelanggaran', [App\Http\Controllers\WalikelasController::class, 'pelanggaranindex'])->name('walikelas-pelanggaran-guru');
    Route::get('/guru/walikelastagihan', [App\Http\Controllers\WalikelasController::class, 'tagihanindex'])->name('walikelas-tagihan-guru');

    
});

//Route Keuangan
Route::group(['middleware'=>'role:keuangan'],function () {
    //generate tagihan
    Route::get('/keuangan/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardkeuangan'])->name('dashboard-keuangan');
    Route::get('/keuangan/generatespp', [App\Http\Controllers\TagihanController::class, 'indextagihan'])->name('generatetagihan-keuangan');
    Route::get('/keuangan/generatespp/{id}', [App\Http\Controllers\TagihanController::class, 'formgenerate'])->name('formgenerate-keuangan');
    Route::post('/keuangan/generatespp/{id}/save', [App\Http\Controllers\TagihanController::class, 'saveformgenerate'])->name('saveformgenerate-keuangan');

    //data tagihan - terima pembayaran
    Route::get('/keuangan/datatagihan', [App\Http\Controllers\TagihanController::class, 'indexdatatagihan'])->name('datatagihan-keuangan');
    Route::post('/keuangan/datatagihan/selectedclass', [App\Http\Controllers\TagihanController::class, 'selectedclass'])->name('selectedclass-keuangan');
    Route::get('/keuangan/datatagihan/selectedclass/{id_kelas}', [App\Http\Controllers\TagihanController::class, 'tagihansiswakelas'])->name('tagihansiswakelas-keuangan');
    Route::post('/keuangan/datatagihan/terimapembayaran', [App\Http\Controllers\TagihanController::class, 'terimapembayaran'])->name('terimapembayaran-keuangan');

    //Laporan
    Route::get('/keuangan/laporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexcreatelaporan'])->name('indexcreatelaporan-keuangan');
    Route::get('/keuangan/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'formlaporan'])->name('formlaporan-keuangan');
    Route::post('/keuangan/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('formlaporan-keuangan');

    //pengajuan
    Route::get('/keuangan/pengajuansarpras', [App\Http\Controllers\PengajuanController::class, 'indexkeuangan'])->name('indexpengajuan-keuangan');
    Route::get('/keuangan/pengajuansarpras/acc/{id}', [App\Http\Controllers\PengajuanController::class, 'accpengajuankeuangan'])->name('accpengajuanpengajuan-keuangan');
    Route::get('/keuangan/pengajuansarpras/reject/{id}', [App\Http\Controllers\PengajuanController::class, 'rejectpengajuankeuangan'])->name('rejectpengajuanpengajuan-keuangan');



});

//Route Sarana Prasarana
Route::group(['middleware'=>'role:sarpras'],function () {
    Route::get('/sarpras/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardsarpras'])->name('dashboard-sarpras');

    //Data Sarpras
    Route::get('/sarpras/datasarpras', [App\Http\Controllers\SarprasController::class, 'indexdatasarpras'])->name('indexdatasarpras-sarpras');
    Route::get('/sarpras/datasarpras/tambah-sarpras', [App\Http\Controllers\SarprasController::class, 'indexadddatasarpras'])->name('indexadddatasarpras-sarpras');
    Route::post('/sarpras/datasarpras/tambah-sarpras/add', [App\Http\Controllers\SarprasController::class, 'adddatasarpras'])->name('adddatasarpras-sarpras');

    //Data Laporan Sarpras
    Route::get('/sarpras/datalaporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexdatalaporansarpras'])->name('indexdatalaporansarpras-sarpras');

    //Pengajuan Sarpras
    Route::get('/sarpras/pengajuan', [App\Http\Controllers\PengajuanController::class, 'index'])->name('indexdatapengajuansarpras-sarpras');
    Route::post('/sarpras/pengajuan/create', [App\Http\Controllers\PengajuanController::class, 'form'])->name('formpengajuansarpras-sarpras');
    Route::post('/sarpras/pengajuan/store', [App\Http\Controllers\PengajuanController::class, 'add'])->name('addpengajuansarpras-sarpras');


});

//Route Kesiswaan
Route::group(['middleware'=>'role:kesiswaan'],function () {
    Route::get('/kesiswaan/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardkesiswaan'])->name('dashboard-kesiswaan');

    //Akun Siswa
    Route::get('/kesiswaan/akunsiswa', [App\Http\Controllers\AccountController::class, 'indexakunsiswa'])->name('akunsiswa-kesiswaan');
    Route::get('/kesiswaan/akunsiswa/selectedclass/{id_kelas}', [App\Http\Controllers\AccountController::class, 'indexakunsiswakelas'])->name('akunsiswakelas-kesiswaan');
    Route::get('/kesiswaan/akunsiswa/tambah-siswa', [App\Http\Controllers\AccountController::class, 'indexaddakunsiswa'])->name('tambahakunsiswa-kesiswaan');
    Route::post('/kesiswaan/akunsiswa/add', [App\Http\Controllers\AccountController::class, 'addakunsiswa'])->name('addakunsiswa-kesiswaan');
    Route::post('/kesiswaan/akunsiswa/selectedclass', [App\Http\Controllers\AccountController::class, 'selectedclass'])->name('selectedclass-kesiswaan');
    Route::post('/kesiswaan/akunsiswa/importexcel', [App\Http\Controllers\AccountController::class, 'importsiswa'])->name('selectedclass-kesiswaan');
    
    //Kelas
    Route::get('/kesiswaan/kelas', [App\Http\Controllers\ClassController::class, 'indexkelas'])->name('indexkelas-kesiswaan');
    Route::get('/kesiswaan/kelas/tambah-kelas', [App\Http\Controllers\ClassController::class, 'indexaddkelas'])->name('indexaddkelas-kesiswaan');
    Route::post('/kesiswaan/kelas/add', [App\Http\Controllers\ClassController::class, 'addkelas'])->name('addkelas-kesiswaan');

    //Akun Guru
    Route::get('/kesiswaan/akunguru', [App\Http\Controllers\AccountController::class, 'indexakunguru'])->name('akunguru-kesiswaan');
    Route::get('/kesiswaan/akunguru/tambah-guru', [App\Http\Controllers\AccountController::class, 'indexaddakunguru'])->name('tambahakunguru-kesiswaan');
    Route::post('/kesiswaan/akunguru/add', [App\Http\Controllers\AccountController::class, 'addakunguru'])->name('addakunguru-kesiswaan');

    //Setting Semester
    Route::get('/kesiswaan/semester', [App\Http\Controllers\SemesterController::class, 'index'])->name('semester-kesiswaan');
    Route::get('/kesiswaan/semester/add', [App\Http\Controllers\SemesterController::class, 'indexadd'])->name('addsemester-kesiswaan');
    Route::post('/kesiswaan/semester/add', [App\Http\Controllers\SemesterController::class, 'actionadd'])->name('addsemester-kesiswaan');
    Route::post('/kesiswaan/semester/setactive', [App\Http\Controllers\SemesterController::class, 'setaktif'])->name('addsemester-kesiswaan');
    Route::get('/kesiswaan/semester/generatelogkelas', [App\Http\Controllers\SemesterController::class, 'generatelogkelas'])->name('addsemester-kesiswaan');
    
    //Setting Mapel
    Route::get('/kesiswaan/mapel', [App\Http\Controllers\MapelController::class, 'index'])->name('mapel-kesiswaan');
    Route::get('/kesiswaan/mapel/{id}', [App\Http\Controllers\MapelController::class, 'indexkelas'])->name('mapelkelas-kesiswaan');
    Route::get('/kesiswaan/mapel/{id}/add', [App\Http\Controllers\MapelController::class, 'formmapel'])->name('addmapelkelas-kesiswaan');
    Route::post('/kesiswaan/mapel/{id}/add', [App\Http\Controllers\MapelController::class, 'addmapel'])->name('addmapelkelas-kesiswaan');

    //Absensi
    Route::get('/kesiswaan/absensi', [App\Http\Controllers\AbsensiController::class, 'index'])->name('absensi-kesiswaan');
    Route::post('/kesiswaan/absensi/importexcel', [App\Http\Controllers\AbsensiController::class, 'importabsensi'])->name('importabsensi-kesiswaan');

    //AdmisiSiswa
    Route::get('/kesiswaan/lulus', [App\Http\Controllers\AdmisiController::class, 'indexlulus'])->name('indexlulus-kesiswaan');
    Route::post('/kesiswaan/lulus/selectedclass', [App\Http\Controllers\AdmisiController::class, 'detaillulus'])->name('detaillulus-kesiswaan');
    Route::post('/kesiswaan/lulus/setlulus', [App\Http\Controllers\AdmisiController::class, 'setlulus'])->name('setlulus-kesiswaan');
    Route::get('/kesiswaan/naikkelas', [App\Http\Controllers\AdmisiController::class, 'indexnaikkelas'])->name('indexnaikkelas-kesiswaan');
    Route::post('/kesiswaan/naikkelas/selectedclass', [App\Http\Controllers\AdmisiController::class, 'detailnaikkelas'])->name('detailnaikkelas-kesiswaan');
    Route::post('/kesiswaan/naikkelas/setnaikkelas', [App\Http\Controllers\AdmisiController::class, 'setnaikkelas'])->name('setnaikkelas-kesiswaan');

    //Setting Wali Kelas
    Route::get('/kesiswaan/walikelas', [App\Http\Controllers\WalikelasController::class, 'indexkesiswaan'])->name('indexwalikelas-kesiswaan');
    Route::post('/kesiswaan/walikelas/set', [App\Http\Controllers\WalikelasController::class, 'setwalikelas'])->name('setwalikelas-kesiswaan');






});

//Route Bimbingan Konseling
Route::group(['middleware'=>'role:bk'],function () {
    
    Route::get('/bk/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardbk'])->name('dashboard-bk');

    //Laporan
    Route::get('/bk/laporansarpras', [App\Http\Controllers\LaporanSarprasController::class, 'indexcreatelaporan'])->name('indexcreatelaporan-bk');
    Route::get('/bk/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'formlaporan'])->name('formlaporan-bk');
    Route::post('/bk/laporansarpras/form', [App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('formlaporan-bk');

    //PelanggaranKBM
    Route::get('/bk/pelanggarankbm', [App\Http\Controllers\PelanggaranKbmController::class, 'managepelanggranbk'])->name('managepelanggarankbm-bk');
    Route::post('/bk/pelanggarankbm/action', [App\Http\Controllers\PelanggaranKbmController::class, 'actionpelanggranbk'])->name('actionpelanggranbk-bk');

    //PelanggaranNonKBM
    Route::get('/bk/pelanggrarannonkbm', [App\Http\Controllers\PelanggaranNonKbmController::class, 'index'])->name('managepelanggarannonkbm-bk');
    Route::post('/bk/pelanggrarannonkbm/create', [App\Http\Controllers\PelanggaranNonKbmController::class, 'form'])->name('formmanagepelanggarannonkbm-bk');
    Route::post('/bk/pelanggrarannonkbm/store', [App\Http\Controllers\PelanggaranNonKbmController::class, 'store'])->name('inputmanagepelanggarannonkbm-bk');
});

require __DIR__.'/auth.php';
