@extends('layout.master')
@section('title')
    Data Kelas - Kesiswaan
@endsection
@section('title-konten')
    Tambah kelas
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/kelas/add">
    @csrf
    <div class="form-group">
      <label for="namakelas">Nama Kelas</label>
      <input type="" class="form-control" name="nama" id="namakelas" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="tingkatkelas">Tingkat</label>
        <select class="form-control" name="tingkat" id="tingkatkelas">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection