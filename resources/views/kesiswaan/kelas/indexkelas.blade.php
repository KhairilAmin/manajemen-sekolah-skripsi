@extends('layout.master')
@section('title')
    Tambah Kelas - Kesiswaan
@endsection
@section('title-konten')
    Data kelas
@endsection
@section('konten')
<a href="/kesiswaan/kelas/tambah-kelas" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Kelas</a>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Tingkat Kelas</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Tingkat Kelas</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($kelas as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->tingkat}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection