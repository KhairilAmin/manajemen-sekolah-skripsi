@extends('layout.master')
@section('title')
    Set Lulus Siswa - Kesiswaan
@endsection
@section('title-konten')
    Piih kelas siswa - Kesiswaan
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/lulus/selectedclass">
    @csrf
    <div class="form-group">
        <label for="kelas">Kelas</label>
        <select class="form-control" name="id_kelas" id="kelas">
            @foreach ($kelas as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection