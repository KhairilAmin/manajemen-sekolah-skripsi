@extends('layout.master')
@section('title')
    Set Lulus - Kesiswaan
@endsection
@section('title-konten')
    Siswa kelas 
@endsection
@section('konten')
    <form method="POST" action="/kesiswaan/lulus/selectedclass">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option {{ $item->id == $id_kelas ? "selected" : "" }} value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
    <form action="/kesiswaan/lulus/setlulus" method="POST">
        @csrf
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIS Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Pilih</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>NIS Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Pilih</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($siswa as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->nis_siswa}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->kelas->nama}}</td>
                            <td><input type="checkbox" name="id[]" value="{{$item->id}}"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Set Lulus</button>
    </form>
@endsection