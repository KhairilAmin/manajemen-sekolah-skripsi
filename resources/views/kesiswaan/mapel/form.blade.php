@extends('layout.master')
@section('title')
    Mapel - Kesiswaan
@endsection
@section('title-konten')
    Tambah Mapel
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/mapel/{{$kelas->id}}/add">
    @csrf
    <div class="form-group">
      <label for="namamapel">Nama Mapel</label>
      <input type="" class="form-control" name="nama" id="namamapel" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group" style="display: none">
        <label for="kelas">Kelas</label>
        <input type="" class="form-control" value="{{$kelas->id}}" name="id_kelas" id="kelas" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group" style="display: none">
        <label for="semester">Kelas</label>
        <input type="" class="form-control" value="{{$semester->id}}" name="id_semester" id="semester" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="pengajar">Guru Pengajar</label>
        <select class="form-control" name="id_guru" id="pengajar">
          @foreach ($guru as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
          @endforeach
        </select>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection