@extends('layout.master')
@section('title')
    Mata Pelajaran - Kesiswaan
@endsection
@section('title-konten')
    Data kelas
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Lihat Mapel</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Lihat Mapel</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($kelas as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td><a href="/kesiswaan/mapel/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Lihat Mapel</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection