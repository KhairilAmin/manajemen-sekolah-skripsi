@extends('layout.master')
@section('title')
    Tambah Kelas - Kesiswaan
@endsection
@section('title-konten')
    Data kelas
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Wali Kelas</th>
                <th>Setting Wali Kelas</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Wali Kelas</th>
                <th>Setting Wali Kelas</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($kelas as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    @php
                        $walikelas = App\Models\Walikelas::where('id_kelas', $item->id)->first();
                    @endphp
                    @if ($walikelas)
                        <td>{{$walikelas->guru->user->name}}</td>
                    @else
                        <td>Wali Kelas belum diatur</td>
                    @endif
                    <td>
                        <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm mb-3" data-toggle="modal" data-target="#exampleModal{{$item->id}}">
                            Setting Wali Kelas
                        </button>
                      
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Set Wali Kelas {{$item->nama}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="/kesiswaan/walikelas/set">
                                        @csrf
                                        <div class="form-group">
                                            <label for="walikelas">Pilih Wali Kelas</label>
                                            <select class="form-control" name="walikelas" id="walikelas">
                                                @foreach ($guru as $itemguru)
                                                    <option value="{{$itemguru->id}}">{{$itemguru->user->name}}</option>
                                                @endforeach
                                            </select>
                                            <input type="" class="form-control" name="id_kelas" id="id_kelas" value="{{$item->id}}" placeholder="Masukkan nama kelas" style="display: none">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Set Wali Kelas</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection