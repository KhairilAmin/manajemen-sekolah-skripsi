@extends('layout.master')
@section('title')
    Dashboard - Kesiswaan
@endsection
@section('title-konten')
    Tambah kelas
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/akunguru/add">
    @csrf
    <div class="form-group">
      <label for="namaguru">Nama Guru</label>
      <input type="" class="form-control" name="nama" id="namaguru" placeholder="Masukkan nama guru">
    </div>
    <div class="form-group">
        <label for="nipguru">NIP/NIPPPK Guru</label>
        <input type="" class="form-control" name="nip" id="nipguru" placeholder="Masukkan NIP/NIPPPK guru">
    </div>
    <div class="form-group">
        <label for="emailguru">Email Guru</label>
        <input type="email" class="form-control" name="email" id="emailguru" placeholder="Masukkan email guru">
    </div>
    <div class="form-group" style="display: none">
        <label for="passwordsiswa">Password</label>
        <input type="" class="form-control" name="password" value="guru1234" id="passwordsiswa" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="role">Role</label>
        <select class="form-control" name="role" id="role">
            <option value="guru">Guru Pengajar</option>
            <option value="keuangan">Keuangan</option>
            <option value="bk">Bimbingan Konseling</option>
            <option value="sarpras">Sarana Prasarana</option>
            <option value="kesiswaan">Kesiswaan</option>
        </select>
    </div>
    <div class="form-group">
        <label for="alamatguru">Alamat Guru</label>
        <input type="" class="form-control" name="alamat" id="alamatguru" placeholder="Masukkan alamat siswa">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection