@extends('layout.master')
@section('title')
    Data Akun Guru - Kesiswaan
@endsection
@section('title-konten')
    Data Akun Guru
@endsection
@section('konten')
    <a href="/kesiswaan/akunguru/tambah-guru" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Guru</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIP/NIPPK Guru</th>
                    <th>Nama Guru</th>
                    <th>Role Guru</th>
                    <th>Email</th>
                    <th>Alamat Guru</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>NIP/NIPPK Guru</th>
                    <th>Nama Guru</th>
                    <th>Role Guru</th>
                    <th>Email</th>
                    <th>Alamat Guru</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($guru as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->nip}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->user->role}}</td>
                        <td>{{$item->user->email}}</td>
                        <td>{{$item->alamat}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection