@extends('layout.master')
@section('title')
    Input Tanggal Absensi - Kesiswaan
@endsection
@section('title-konten')
    Input Tanggal Absensi
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/absensi/importexcel" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="kelas">Pilih File</label> <br>
        <input type="file" name="excel_absensi" />
    </div>
    <button type="submit" class="btn btn-primary">Upload</button>
</form>
@endsection