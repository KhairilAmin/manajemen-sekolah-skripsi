@extends('layout.master')
@section('title')
    Semester - Kesiswaan
@endsection
@section('title-konten')
    Tambah Data Semester
@endsection
@section('konten')
    <form method="POST" action="/kesiswaan/semester/add">
        @csrf
        <div class="form-group">
            <label for="nama">Nama Semester <small>( Contoh : Ganjil 2022/2023 )</small></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama semester">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection