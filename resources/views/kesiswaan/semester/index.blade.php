@extends('layout.master')
@section('title')
    Semester - Kesiswaan
@endsection
@section('title-konten')
    Data Semester
@endsection
@section('konten')
    <a href="/kesiswaan/semester/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Semester</a>
    <form method="POST" action="/kesiswaan/semester/setactive">
        @csrf
        <div class="form-group">
            <label for="semster">Pilih Semester Aktif</label>
            <select class="form-control" name="id_semester" id="semster">
                @foreach ($semester as $item)
                    <option {{ $item->is_aktif == 1 ? "selected" : "" }} value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br>
    <p>Generate log kelas sebelum mengganti semester, untuk menyimpan nilai siswa pada semester dan kelas sebelumnya</p>
    <a href="/kesiswaan/semester/generatelogkelas" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Generate Log Kelas</a>
@endsection