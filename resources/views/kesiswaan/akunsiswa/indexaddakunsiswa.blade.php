@extends('layout.master')
@section('title')
    Dashboard - Kesiswaan
@endsection
@section('title-konten')
    Tambah siswa
@endsection
@section('konten')
<form method="POST" action="/kesiswaan/akunsiswa/add">
    @csrf
    <div class="form-group">
      <label for="namasiswa">Nama Siswa</label>
      <input type="" class="form-control" name="nama" id="namasiswa" placeholder="Masukkan nama siswa">
    </div>
    <div class="form-group">
        <label for="nissiswa">NIS Siswa</label>
        <input type="" class="form-control" name="nis_siswa" id="nissiswa" placeholder="Masukkan NIS siswa">
    </div>
    <div class="form-group">
        <label for="emailsiswa">Email Siswa</label>
        <input type="email" class="form-control" name="email" id="emailsiswa" placeholder="Masukkan email siswa">
    </div>
    <div class="form-group" style="display: none">
        <label for="passwordsiswa">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordsiswa" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="kelas">Kelas</label>
        <select class="form-control" name="id_kelas" id="kelas">
            @foreach ($kelas as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="alamatsiswa">Alamat Siswa</label>
        <input type="" class="form-control" name="alamat" id="alamatsiswa" placeholder="Masukkan alamat siswa">
    </div>
    <div class="form-group">
        <label for="namawalimurid">Nama Wali Murid</label>
        <input type="" class="form-control" name="namawali" id="namawalimurid" placeholder="Masukkan nama wali murid">
      </div>
      <div class="form-group">
          <label for="nohpwalimurid">No Hp Wali Murid</label>
          <input type="" class="form-control" name="nohp" id="nohpwalimurid" placeholder="Masukkan no hp walimurid">
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection