@extends('layout.master')
@section('title')
    Dashboard - Kesiswaan
@endsection
@section('title-konten')
    Data Akun Siswa
@endsection
@section('konten')
    <a href="/kesiswaan/akunsiswa/tambah-siswa" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Siswa</a>
    <!-- Button trigger modal -->
    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm mb-3" data-toggle="modal" data-target="#exampleModal">
        Import Excel
    </button>
  
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/kesiswaan/akunsiswa/importexcel" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="kelas">Pilih File</label> <br>
                        <input type="file" name="excel_siswa" />
                    </div>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
        </div>
    </div>
    <form method="POST" action="/kesiswaan/akunsiswa/selectedclass">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection