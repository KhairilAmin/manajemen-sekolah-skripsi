@extends('layout.master')
@section('title')
    Dashboard - Kesiswaan
@endsection
@section('title-konten')
    Data Akun Siswa
@endsection
@section('konten')
    <a href="/kesiswaan/akunsiswa/tambah-siswa" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Siswa</a>
    <form method="POST" action="/kesiswaan/akunsiswa/selectedclass">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option {{ $item->id == $id_kelas ? "selected" : "" }} value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIS Siswa</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Email Siswa</th>
                    <th>Alamat Siswa</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>NIS Siswa</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Email Siswa</th>
                    <th>Alamat Siswa</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($siswa as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->nis_siswa}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->kelas->nama}}</td>
                        <td>{{$item->user->email}}</td>
                        <td>{{$item->alamat}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection