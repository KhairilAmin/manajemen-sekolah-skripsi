@extends('layout.master')
@section('title')
Data Pembayaran - Siswa
@endsection
@section('title-konten')
    Data Pembayaran - Siswa ( {{$siswa->nama}} )
@endsection
@section('konten')
<h6>Pembayaran Lunas</h6>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Tagihan SPP</th>
                <th>Tanggal Terakhir</th>
                <th>Nominal</th>
                <th>Status Pembayaran</th>
                <th>Tanggal Pembayaran</th>
                <th>Penerima Pembayaran</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Tagihan SPP</th>
                <th>Tanggal Terakhir</th>
                <th>Nominal</th>
                <th>Status Pembayaran</th>
                <th>Tanggal Pembayaran</th>
                <th>Penerima Pembayaran</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pembayaranselesai as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->offdate}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{ $item->is_tagih == 1 ? "Belum Lunas" : "Lunas" }}</td>
                    <td>{{$item->updated_at->format('Y-m-d')}}</td>
                    <td>{{$item->penerima->name}}</td>                    
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<h6 class="mt-5">Pembayaran Belum Lunas</h6>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Tagihan SPP</th>
                <th>Tanggal Terakhir</th>
                <th>Nominal</th>
                <th>Status Pembayaran</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Tagihan SPP</th>
                <th>Tanggal Terakhir</th>
                <th>Nominal</th>
                <th>Status Pembayaran</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pembayaranbelum as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->offdate}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{ $item->is_tagih == 1 ? "Belum Lunas" : "Lunas" }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection