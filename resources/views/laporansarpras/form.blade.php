@extends('layout.master')
@section('title')
    Dashboard - Kesiswaan
@endsection
@section('title-konten')
    Tambah kelas
@endsection
@section('konten')
<form method="POST" action="/{{$user->role}}/laporansarpras/form">
    @csrf
    <div class="form-group">
        <label for="kelas">Sarana Prasarana</label>
        <select class="form-control" name="id_sarpras" id="kelas">
            @foreach ($sarpras as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="kelas">Tingat Kerusakan</label>
        <select class="form-control" name="tingkat_kerusakan" id="kelas">
            <option value="ringan">Ringan</option>
            <option value="sedang">Sedang</option>
            <option value="berat">Berat</option>
        </select>
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="textarea" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan laporan">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection