@extends('layout.master')
@section('title')
    Data Laporan Sarpras - {{$user->role}}
@endsection
@section('title-konten')
    Laporan saya ( {{$user->name}} ) 
@endsection
@section('konten')
    <a href="/{{$user->role}}/laporansarpras/form" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Buat Laporan</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sarana Prasarana</th>
                    <th>Nama Pelapor</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Sarana Prasarana</th>
                    <th>Nama Pelapor</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($laporan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->sarpras->nama}}</td>
                        <td>{{$item->pelapor->name}}</td>
                        <td>{{$item->tingkat_kerusakan}}</td>
                        <td>{{$item->keterangan}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection