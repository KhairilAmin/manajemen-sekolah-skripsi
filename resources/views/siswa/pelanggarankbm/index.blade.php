@extends('layout.master')
@section('title')
    Pelanggaran KBM - {{$auth->role}}
@endsection
@section('title-konten')
    Pelanggaran KBM - {{$auth->role}}
@endsection
@section('konten')
<h5>Pelanggaran Belum ditindak</h5>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pelanggaranbelum as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->siswa->nama}}</td>
                    <td>{{$item->mapel->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->tanggal_pelanggaran}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br>
<br>

<h5>Pelanggaran Selesai ditindak</h5>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Penindak</th>
                <th>Poin</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Penindak</th>
                <th>Poin</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pelanggaranselesai as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->siswa->nama}}</td>
                    <td>{{$item->mapel->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->tanggal_pelanggaran}}</td>
                    <td>{{$item->bk->nama}}</td>
                    <td>{{$item->poin}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection