@extends('layout.master')
@section('title')
    Rapor - {{$auth->role}}
@endsection
@section('title-konten')
    Rapor - {{$log->semester->nama}} ( {{$log->kelas->nama}} )
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Nilai Rapor</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Nilai Rapor</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($mapel as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->pengajar->name}}</td>
                    <td>{{$item->kelas->nama}}</td>
                    @php
                        $nilai = App\Models\Rapor::where('id_siswa',$siswa->id)->where('id_mapel',$item->id)->first();
                    @endphp
                    @if ($nilai)
                        <td>{{$nilai->nilai_huruf}} - ({{$nilai->nilai_angka}})</td>
                    @else
                        <td>Nilai belum ada</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection