@extends('layout.master')
@section('title')
    Nilai Rapor - {{$auth->role}}
@endsection
@section('title-konten')
    Rapor
@endsection
@section('konten')
<form method="POST" action="/{{$auth->role}}/rapor">
    @csrf
    <div class="form-group">
        <label for="log">Semester</label>
        <select class="form-control" name="log" id="log">
            @foreach ($log as $item)
                <option value="{{$item->id}}">{{$item->semester->nama}} - {{$item->kelas->nama}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection