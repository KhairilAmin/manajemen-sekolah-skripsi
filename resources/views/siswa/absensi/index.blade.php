@extends('layout.master')
@section('title')
    Absensi - {{$auth->role}}
@endsection
@section('title-konten')
    Absensi
@endsection
@section('konten')
    <form method="POST" action="/siswa/absensi">
        @csrf
        <div class="form-group">
            <label for="bulan">Bulan</label>
            <select class="form-control" name="bulan" id="bulan">
                <option value="Januari">Januari</option>
                <option value="Februari">Februari</option>
                <option value="Maret">Maret</option>
                <option value="April">April</option>
                <option value="Mei">Mei</option>
                <option value="Juni">Juni</option>
                <option value="Juli">Juli</option>
                <option value="Agustus">Agustus</option>
                <option value="September">September</option>
                <option value="Oktober">Oktober</option>
                <option value="November">November</option>
                <option value="Desember">Desember</option>
                
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
@endsection