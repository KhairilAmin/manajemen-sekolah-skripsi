@extends('layout.master')
@section('title')
    Absensi - {{$auth->role}}
@endsection
@section('title-konten')
    Absensi
@endsection
@section('konten')
    <form method="POST" action="/siswa/absensi">
        @csrf
        <div class="form-group">
            <label for="bulan">Bulan</label>
            <select class="form-control" name="bulan" id="bulan">
                <option {{ $bulan == "Januari" ? "selected" : "" }} value="Januari">Januari</option>
                <option {{ $bulan == "Februari" ? "selected" : "" }} value="Februari">Februari</option>
                <option {{ $bulan == "Maret" ? "selected" : "" }} value="Maret">Maret</option>
                <option {{ $bulan == "April" ? "selected" : "" }} value="April">April</option>
                <option {{ $bulan == "Mei" ? "selected" : "" }} value="Mei">Mei</option>
                <option {{ $bulan == "Juni" ? "selected" : "" }} value="Juni">Juni</option>
                <option {{ $bulan == "Juli" ? "selected" : "" }} value="Juli">Juli</option>
                <option {{ $bulan == "Agustus" ? "selected" : "" }} value="Agustus">Agustus</option>
                <option {{ $bulan == "September" ? "selected" : "" }} value="September">September</option>
                <option {{ $bulan == "Oktober" ? "selected" : "" }} value="Oktober">Oktober</option>
                <option {{ $bulan == "November" ? "selected" : "" }} value="November">November</option>
                <option {{ $bulan == "Desember" ? "selected" : "" }} value="Desember">Desember</option>
                
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Hari</th>
                <th>Keterangan</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tanggal as $item)
                <tr>
                    <td>{{$item->tanggal}}</td>
                    <td>{{$item->hari}}</td>
                    <td>{{$item->keterangan}}</td>
                    @if ($item->statushari == 1)
                        @php
                            $absensisiswa = App\Models\AbsensiSiswa::where('id_tanggal_absensi',$item->id)->where('id_siswa',$siswa->id)->first();
                        @endphp
                        @if ($absensisiswa->status == 1)
                            <td>Hadir</td>
                        @elseif($absensisiswa->status == 2)
                            <td>Izin</td>
                        @elseif($absensisiswa->status == 3)
                            <td>Sakit</td>
                        @elseif($absensisiswa->status == 4)
                            <td>Alpa</td>
                        @elseif($absensisiswa->status == 0)
                            <td>Absensi belum di isi</td>
                        @endif
                    @else
                        <td>Libur</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection