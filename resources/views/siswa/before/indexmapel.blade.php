@extends('layout.master')
@section('title')
    Mata Nilai - {{$auth->role}}
@endsection
@section('title-konten')
    Data Mapel - {{$log->semester->nama}} ( {{$log->kelas->nama}} )
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Tambah Komponen Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Tambah Komponen Nilai</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($mapel as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->pengajar->name}}</td>
                    <td>{{$item->kelas->nama}}</td>
                    <td><a href="/{{$auth->role}}/nilaisebelumnya/{{$item->id}}/{{$log->id}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Lihat Nilai</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection