@extends('layout.master')
@section('title')
    Mata Nilai - {{$auth->role}}
@endsection
@section('title-konten')
    Data Nilai {{$mapel->nama}} - {{$log->semester->nama}} ( {{$log->kelas->nama}} )
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Komponen Mapel</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Komponen Mapel</th>
                <th>Nilai</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($komponen as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    @php
                        $nilai = App\Models\NilaiSiswa::where('id_komponen',$item->id)->where('id_siswa',$siswa->id)->first();
                        if ($nilai) {
                            $angka = $nilai->nilai;
                        }else {
                            $angka = 'Belum ada nilai';
                        }
                    @endphp
                    <td>{{$angka}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection