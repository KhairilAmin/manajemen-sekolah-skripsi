@extends('layout.master')
@section('title')
    Pelanggaran non KBM - BK
@endsection
@section('title-konten')
    Buat Laporan Pelanggaran
@endsection
@section('konten')
    <form method="POST" action="/bk/pelanggrarannonkbm/store">
        @csrf
        <div class="form-group">
            <label for="id_siswa">Nama</label>
            <select class="form-control" name="id_siswa" id="id_siswa">
                @foreach ($siswa as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="keterangan">Keterangan Pelanggaran</label>
            <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan pelanggaran">
        </div>
        <div class="form-group" style="display: none">
            <label for="id_bk">id_bk</label>
            <input type="" value="{{$guru->id}}" class="form-control" name="id_bk" id="id_bk" placeholder="id_bk nilai dalam rapor">
        </div>
        <div class="form-group">
            <label for="tanggal_pelanggaran">Tanggal Pelanggaran</label>
            <input class="form-control" name="tanggal_pelanggaran" data-date-format="yyyy/mm/dd" id="datepicker">
        </div>
        <div class="form-group">
            <label for="poin">Poin</label>
            <input type="number" class="form-control" name="poin" id="poin" placeholder="Poin pelanggaran">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection