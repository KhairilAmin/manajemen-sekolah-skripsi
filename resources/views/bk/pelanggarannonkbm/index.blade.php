@extends('layout.master')
@section('title')
    Pelanggaran non KBM - BK
@endsection
@section('title-konten')
    Pilih Kelas
@endsection
@section('konten')
    <form method="POST" action="/bk/pelanggrarannonkbm/create">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>BK</th>
                    <th>Poin</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>BK</th>
                    <th>Poin</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($pelanggaran as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->siswa->nama}}</td>
                        <td>{{$item->siswa->kelas->nama}}</td>
                        <td>{{$item->keterangan}}</td>
                        <td>{{$item->tanggal_pelanggaran}}</td>
                        <td>{{$item->bk->nama}}</td>
                        <td>{{$item->poin}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection