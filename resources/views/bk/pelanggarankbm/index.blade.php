@extends('layout.master')
@section('title')
    Pelanggaran KBM - Bimbingan Konseling
@endsection
@section('title-konten')
    Pelanggaran KBM - Bimbingan Konseling
@endsection
@section('konten')
<h5>Pelanggaran Belum ditindak</h5>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pelanggaranbelum as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->siswa->nama}}</td>
                    <td>{{$item->mapel->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->tanggal_pelanggaran}}</td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Tindak
                        </button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form method="POST" action="/bk/pelanggarankbm/action">
                                @csrf
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Tindak Pelanggaran</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" name="id" value="{{$item->id}}" id="" style="display: none">
                                    <div class="form-group">
                                        <label for="poin">Poin</label>
                                        <input type="number" class="form-control" name="poin" id="keterangan" placeholder="Poin pelanggaran">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>    
                    </td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<br>
<br>

<h5>Pelanggaran Selesai ditindak</h5>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Penindak</th>
                <th>Poin</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Mapel</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>Penindak</th>
                <th>Poin</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pelanggaranselesai as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->siswa->nama}}</td>
                    <td>{{$item->mapel->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->tanggal_pelanggaran}}</td>
                    <td>{{$item->bk->nama}}</td>
                    <td>{{$item->poin}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection