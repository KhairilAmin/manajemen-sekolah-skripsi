@extends('layout.master')
@section('title')
    Data Sarana Prasarana - Sarana Prasarana
@endsection
@section('title-konten')
    Data Sarana Prasarana
@endsection
@section('konten')
    <a href="/sarpras/datasarpras/tambah-sarpras" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Sarana Prasarana</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Sarana Prasarana</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($laporan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->sarpras->nama}}</td>
                        <td>{{$item->tingkat_kerusakan}}</td>
                        <td>{{$item->keterangan}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection