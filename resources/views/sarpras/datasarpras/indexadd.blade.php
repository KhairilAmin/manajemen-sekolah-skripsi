@extends('layout.master')
@section('title')
    Tambah Sarana Prasarana - Kesiswaan
@endsection
@section('title-konten')
    Tambah Sarana Prasarana
@endsection
@section('konten')
<form method="POST" action="/sarpras/datasarpras/tambah-sarpras/add">
    @csrf
    <div class="form-group">
      <label for="nama">Nama Sarana Prasarana</label>
      <input type="" class="form-control" name="nama" id="nama" placeholder="Masukkan nama sarana prasarana">
    </div>
    <div class="form-group">
        <label for="jumlah">Jumlah Sarana Prasarana</label>
        <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan Jumlah Sarana Prasarana">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection