@extends('layout.master')
@section('title')
    Data Sarana Prasarana - Sarana Prasarana
@endsection
@section('title-konten')
    Data Sarana Prasarana
@endsection
@section('konten')
    <a href="/sarpras/datasarpras/tambah-sarpras" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>NIS Siswa</th>
                    <th>Nama Siswa</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($sarpras as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->jumlah}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection