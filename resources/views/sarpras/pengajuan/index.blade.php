@extends('layout.master')
@section('title')
    Data Pengajuan Sarana Prasarana - Sarana Prasarana
@endsection
@section('title-konten')
    Data Pengajuan Sarana Prasarana
@endsection
@section('konten')
<form method="POST" action="/sarpras/pengajuan/create">
    @csrf
    <div class="form-group">
        <label for="sarpras">Pilih Sarana Prasarana</label>
        <select class="form-control" name="id_sarpras" id="sarpras">
            @foreach ($sarpras as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Tambah Pengajuan</button>
</form>
<br>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pengajuan as $key => $item)
                <tr>
                    <td>{{$key +1 }}</td>
                    <td>{{$item->sarpras->nama}}</td>
                    <td>{{$item->pengaju->name}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{ $item->status == 0 ? "Belum di ACC" : "Sudah ACC" }}</td>
                    @if ($item->id_penerima)
                        <td>{{$item->penerima->name}}</td>
                    @else
                        <td>Belum di ACC</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
    {{-- <a href="/sarpras/pengajuan/tambah-pengajuan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Pengajuan</a> --}}
@endsection