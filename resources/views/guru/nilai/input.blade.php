@extends('layout.master')
@section('title')
    Komponen Nilai - Guru
@endsection
@section('title-konten')
    Input nilai siswa {{$mapel->nama}} - {{$komponenmapel->nama}}
@endsection
@section('konten')
<form action="/guru/inputnilai/{{$mapel->id}}/input" method="POST">
    @csrf
    <input type="" name="id_komponen" value="{{$komponenmapel->id}}" style="display: none">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIS Siswa</th>
                    <th>Nama Siswa</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>NIS Siswa</th>
                    <th>Nama Siswa</th>
                    <th>Nilai</th>
                </tr>
            </tfoot>
            <tbody>
                @php
                    $nomer = 0;
                @endphp
                @foreach ($siswa as $item)
                        <tr>
                            <td>{{$nomer+1}}</td>
                            <td>{{$item->nis_siswa}}</td>
                            <td>{{$item->nama}}</td>
                            @php
                                $nilai = App\Models\NilaiSiswa::where('id_komponen',$komponenmapel->id)->where('id_siswa',$item->id)->first();
                            @endphp
                            <td>
                                <input type="" name="id_siswa[]" value="{{$item->id}}" style="display: none">
                                @if ($nilai)
                                <input value="{{ old('name', $nilai->nilai) }}" type="" name="nilai[]" value="">
                                @else
                                <input value="" type="" name="nilai[]" value="">
                                @endif
                            </td>
                        </tr>
                        @php
                            $nomer = $nomer + 1;   
                        @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    <button type="submit" class="btn btn-primary mb-3">Simpan Nilai</button>
</form>
@endsection