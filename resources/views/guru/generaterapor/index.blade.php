@extends('layout.master')
@section('title')
    Generate Rapor - Guru
@endsection
@section('title-konten')
    Data Mapel
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Tambah Komponen Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Tambah Komponen Nilai</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($mapel as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->pengajar->name}}</td>
                    <td>{{$item->kelas->nama}}</td>
                    @php
                        $komponen = App\Models\KomponenMapel::where('id_mapel',$item->id)->get();
                        $totalkomponen = 0;
                        foreach ($komponen as $key => $value) {
                            $totalkomponen = $totalkomponen + $value->persentase;
                        }   
                    @endphp
                    @if ($totalkomponen == 100) 
                        <td><a href="/guru/generaterapor/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Generate Nilai Rapor</a></td>
                    @else
                        <td>Persentase nilai mapel belum 100%</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection