@extends('layout.master')
@section('title')
    Tagihan Kelas - Wali Kelas
@endsection
@section('title-konten')
    Data Tagihan {{$kelas->nama}}
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>NIS Siswa</th>
                <th>Nama Siswa</th>
                <th>Tagihan</th>
                <th>Tanggal Terakhir Tagihan</th>
                <th>Nominal</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>NIS Siswa</th>
                <th>Nama Siswa</th>
                <th>Tagihan</th>
                <th>Tanggal Terakhir Tagihan</th>
                <th>Nominal</th>
            </tr>
        </tfoot>
        <tbody>
            @php
                $nomer = 0;
            @endphp
            @foreach ($siswa as $item)
                @foreach ($item->tagihan->where('is_tagih',1) as $tagihan)
                    <tr>
                        <td>{{$nomer+1}}</td>
                        <td>{{$item->nis_siswa}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$tagihan->keterangan}}</td>
                        <td>{{$tagihan->offdate}}</td>
                        <td>{{$tagihan->nominal}}</td>
                    </tr>
                    @php
                        $nomer = $nomer + 1;   
                    @endphp
                @endforeach
            @endforeach
        </tbody>
    </table>
</div>
@endsection