@extends('layout.master')
@section('title')
    Pelanggaran non KBM - Wali Kelas
@endsection
@section('title-konten')
    Pelanggaran Siswa {{$kelas->nama}}
@endsection
@section('konten')
    <h5>Pelanggaran non KBM</h5>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>BK</th>
                    <th>Poin</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>BK</th>
                    <th>Poin</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($siswa as $s)
                    @php
                        $pelanggaran = App\Models\PelaggaranNonKbm::where('id_siswa',$s->id)->first();
                    @endphp
                    @if ($pelanggaran != null)
                        <tr>
                            <td>{{$pelanggaran->siswa->nama}}</td>
                            <td>{{$pelanggaran->siswa->kelas->nama}}</td>
                            <td>{{$pelanggaran->keterangan}}</td>
                            <td>{{$pelanggaran->tanggal_pelanggaran}}</td>
                            <td>{{$pelanggaran->bk->nama}}</td>
                            <td>{{$pelanggaran->poin}}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <h5>Pelanggaran KBM</h5>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Nama Siswa</th>
                    <th>Mapel</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>Penindak</th>
                    <th>Poin</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Nama Siswa</th>
                    <th>Mapel</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pelanggaran</th>
                    <th>Penindak</th>
                    <th>Poin</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($siswa as $s)    
                    @php
                        $pelanggarankbm = App\Models\PelanggaranKbm::where('id_siswa',$s->id)->get();
                    @endphp
                    @if ($pelanggarankbm != null)
                        @foreach ($pelanggarankbm as $pkbm)    
                            <tr>
                                <td>{{$pkbm->siswa->nama}}</td>
                                <td>{{$pkbm->mapel->nama}}</td>
                                <td>{{$pkbm->keterangan}}</td>
                                <td>{{$pkbm->tanggal_pelanggaran}}</td>
                                @if ($pkbm->bk != null)
                                    <td>{{$pkbm->bk->nama}}</td>
                                @else
                                    <td>Belum Ditindak</td>   
                                @endif
                                @if ($pkbm->poin != null)
                                    <td>{{$pkbm->poin}}</td>
                                @else
                                    <td>Belum Ditindak</td>   
                                @endif
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection