@extends('layout.master')
@section('title')
    Nilai Rapor - Guru
@endsection
@section('title-konten')
    Nilai Rapor siswa {{$mapel->nama}} - {{$kelas->nama}}
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>NIS Siswa</th>
                <th>Nama Siswa</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>NIS Siswa</th>
                <th>Nama Siswa</th>
                <th>Nilai</th>
            </tr>
        </tfoot>
        <tbody>
            @php
                $nomer = 0;
            @endphp
            @foreach ($siswas as $item)
                    <tr>
                        <td>{{$nomer+1}}</td>
                        <td>{{$item->nis_siswa}}</td>
                        <td>{{$item->nama}}</td>
                        @php
                            $nilai = App\Models\Rapor::where('id_mapel',$mapel->id)
                            ->where('id_siswa',$item->id)->first();
                        @endphp
                        <td>
                            {{$nilai->nilai_angka}} ({{$nilai->nilai_huruf}})
                        </td>
                    </tr>
                    @php
                        $nomer = $nomer + 1;   
                    @endphp
            @endforeach
        </tbody>
    </table>
@endsection