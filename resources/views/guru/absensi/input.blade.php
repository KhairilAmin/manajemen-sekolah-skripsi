@extends('layout.master')
@section('title')
    Absensi - Guru
@endsection
@section('title-konten')
    Data Absensi Siswa
@endsection
@section('konten')
<form action="submitabsensi" method="POST">
    @csrf
    <input type="text" name="tanggal" value="{{$tanggal->id}}" style="display: none">
    <table class="table">
        <thead>
            <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Absensi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($siswa as $item)
                <tr>
                    <td>{{$item->nis_siswa}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->kelas->nama}}</td>
                    @php
                        $absensisiswa = App\Models\AbsensiSiswa::where('id_siswa', $item->id)->where('id_tanggal_absensi',$tanggal->id)->first();
                    @endphp
                    <td>
                        <input type="text" name="id_siswa[]" value="{{$item->id}}" style="display: none">
                        <select name="status[]" id="cars">
                            <option {{ $absensisiswa->status == 1 ? "selected" : "" }}  value="1">Hadir</option>
                            <option {{ $absensisiswa->status == 2 ? "selected" : "" }} value="2">Sakit</option>
                            <option {{ $absensisiswa->status == 3 ? "selected" : "" }} value="3">Izin</option>
                            <option {{ $absensisiswa->status == 4 ? "selected" : "" }} value="4">Alpa</option>
                        </select>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" type="submit">Submit</button>
</form>
@endsection