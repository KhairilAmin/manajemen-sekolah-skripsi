@extends('layout.master')
@section('title')
    Absensi - Guru
@endsection
@section('title-konten')
    Data Tanggal Absensi
@endsection
@section('konten')
<table class="table">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Hari</th>
            <th>Status</th>
            <th>Keterangan</th>
            <th>Isi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tanggalabsensi as $item)
            <tr>
                <td>{{$item->tanggal}}</td>
                <td>{{$item->hari}}</td>
                @if ($item->statushari == 1)
                    <td>Masuk</td>
                @else
                    <td>Libur</td>
                @endif
                <td>{{$item->keterangan}}</td>
                @if ($item->statushari == 1)
                    <td><a class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" href="/guru/absensi/{{$item->id}}/{{$kelas->id}}">Isi Absensi</a></td>
                @else
                    <td>Libur</td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
@endsection