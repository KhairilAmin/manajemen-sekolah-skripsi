@extends('layout.master')
@section('title')
    Pelanggaran KBM - Guru
@endsection
@section('title-konten')
    Data Pelanggaran KBM {{$mapel->nama}} - {{$mapel->kelas->nama}}
@endsection
@section('konten')
<form method="POST" action="/guru/pelanggarankbm/{{$mapel->id}}">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Siswa</label>
        <select class="form-control" name="id_siswa" id="siswa">
            @foreach ($siswa as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="persentase">Keterangan Pelanggaran</label>
        <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan pelanggaran">
    </div>
    <div class="form-group" style="display: none">
        <label for="id_mapel">Id Mapel</label>
        <input type="" value="{{$mapel->id}}" class="form-control" name="id_mapel" id="id_mapel" placeholder="id_mapel nilai dalam rapor">
    </div>
    <div class="form-group">
        <label for="tanggal_pelanggaran">Tanggal Pelanggaran</label>
        <input class="form-control" name="tanggal_pelanggaran" data-date-format="yyyy/mm/dd" id="datepicker">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<br>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>BK</th>
                <th>Poin</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Keterangan</th>
                <th>Tanggal Pelanggaran</th>
                <th>BK</th>
                <th>Poin</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pelanggaran as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->siswa->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->tanggal_pelanggaran}}</td>
                    @if ($item->bk)
                        <td>{{$item->siswa->nama}}</td>
                    @else
                        <td>Belum ditangani BK</td>
                    @endif
                    @if ($item->bk)
                        <td>{{$item->poin}}</td>
                    @else
                        <td>Belum ditangani BK</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection