@extends('layout.master')
@section('title')
    Pelanggaran KBM - Guru
@endsection
@section('title-konten')
    Data Mapel
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Buat Pelanggaran</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Guru Pengajar</th>
                <th>Kelas</th>
                <th>Buat Pelanggaran</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($mapel as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->pengajar->name}}</td>
                    <td>{{$item->kelas->nama}}</td>
                    <td><a href="/guru/pelanggarankbm/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Buat Pelanggaran</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection