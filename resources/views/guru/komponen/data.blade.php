@extends('layout.master')
@section('title')
    Komponen Mata Pelajaran - Guru
@endsection
@section('title-konten')
    Data Komponen Mapel
@endsection
@section('konten')
<a href="/guru/komponennilai/{{$id}}/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Komponen</a>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Komponen</th>
                <th>Persentase dalam rapor</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Komponen</th>
                <th>Persentase dalam rapor</th>
                <th>Edit</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($komponen as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->persentase}}%</td>
                    <td><a href="/guru/komponennilai/{{$item->id}}/edit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Edit Komponen</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection