@extends('layout.master')
@section('title')
    Komponen Nilai - Guru
@endsection
@section('title-konten')
    Tambah komponen nilai {{$mapel->nama}}
@endsection
@section('konten')
<form method="POST" action="/guru/komponennilai/{{$mapel->id}}/add">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Komponen</label>
        <input type="textarea" class="form-control" name="nama" id="nama" placeholder="Masukkan nama komponen">
    </div>
    <div class="form-group">
        <label for="persentase">Persentasi dalam rapor</label>
        <input type="number" class="form-control" name="persentase" id="persentase" placeholder="Persentase nilai dalam rapor">
    </div>
    <div class="form-group" style="display: none">
        <label for="id_mapel">Id Mapel</label>
        <input type="" value="{{$mapel->id}}" class="form-control" name="id_mapel" id="id_mapel" placeholder="id_mapel nilai dalam rapor">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection