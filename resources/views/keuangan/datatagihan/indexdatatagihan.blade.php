@extends('layout.master')
@section('title')
    Data Tagihan - Keuangan
@endsection
@section('title-konten')
    Data Tagihan
@endsection
@section('konten')
    <form method="POST" action="/keuangan/datatagihan/selectedclass">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection