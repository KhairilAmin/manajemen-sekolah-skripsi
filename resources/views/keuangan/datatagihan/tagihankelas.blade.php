@extends('layout.master')
@section('title')
    Tagihan Kelas - Keuangan
@endsection
@section('title-konten')
    Data Tagihan Per-Kelas
@endsection
@section('konten')
    <form method="POST" action="/keuangan/datatagihan/selectedclass">
        @csrf
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <select class="form-control" name="id_kelas" id="kelas">
                @foreach ($kelas as $item)
                    <option {{ $item->id == $id_kelas ? "selected" : "" }} value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
    <form action="/keuangan/datatagihan/terimapembayaran" method="POST">
        @csrf
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIS Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Tagihan</th>
                        <th>Tanggal Terakhir Tagihan</th>
                        <th>Nominal</th>
                        <th>Pilih</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>NIS Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Tagihan</th>
                        <th>Tanggal Terakhir Tagihan</th>
                        <th>Nominal</th>
                        <th>Pilih</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php
                        $nomer = 0;
                    @endphp
                    @foreach ($siswa as $item)
                        @foreach ($item->tagihan->where('is_tagih',1) as $tagihan)
                            <tr>
                                <td>{{$nomer+1}}</td>
                                <td>{{$item->nis_siswa}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$tagihan->keterangan}}</td>
                                <td>{{$tagihan->offdate}}</td>
                                <td>{{$tagihan->nominal}}</td>
                                <td><input type="checkbox" name="id[]" value="{{$tagihan->id}}"></td>
                            </tr>
                            @php
                                $nomer = $nomer + 1;   
                            @endphp
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
        <button type="submit" class="btn btn-primary mb-3">Terima Pembayaran</button>
    </form>
@endsection