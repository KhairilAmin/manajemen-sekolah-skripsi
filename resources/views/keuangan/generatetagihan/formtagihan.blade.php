@extends('layout.master')
@section('title')
    Form Generate Tagihan - Kesiswaan
@endsection
@section('title-konten')
    Form Generate Tagihan
@endsection
@section('konten')
<h4 class="mb-4">Tambah tagihan pada kelas {{$kelas->nama}}</h4>
<form method="POST" action="/keuangan/generatespp/{{$kelas->id}}/save">
    @csrf
    <div class="form-group" style="display: none">
        <label for="kelas">Kelas</label>
        <input type="text" name="kelas" class="form-control" value="{{$kelas->id}}">
    </div>
    <div class="form-group">
        <label for="offdate">Tagihan SPP</label>
        <input class="form-control" name="keterangan" data-date-format="MM/yyyy" id="datepicker2">
    </div>
    <div class="form-group">
        <label for="offdate">Tanggal Terakhir</label>
        <input class="form-control" name="offdate" data-date-format="yyyy/mm/dd" id="datepicker">
    </div>
    <div class="form-group">
      <label for="nominal">Nominal Tagihan</label>
      <input type="" class="form-control" name="nominal" id="nominal" placeholder="Masukkan nominal tagihan">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection