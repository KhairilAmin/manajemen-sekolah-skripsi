@extends('layout.master')
@section('title')
    Generate Tagihan - Keuangan
@endsection
@section('title-konten')
    Generate Tagihan
@endsection
@section('konten')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Tingkat Kelas</th>
                <th>Generate Tagihan</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Tingkat Kelas</th>
                <th>Generate Tagihan</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($kelas as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->tingkat}}</td>
                    <td><a href="/keuangan/generatespp/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Generate Tagihan</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection