@extends('layout.master')
@section('title')
    Data Pengajuan - Keuangan
@endsection
@section('title-konten')
    Data Pengajuan
@endsection
@section('konten')
<a href="/kesiswaan/kelas/tambah-kelas" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Kelas</a>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Laporan Terlampir</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Laporan Terlampir</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pengajuan as $key => $item)
                <tr>
                    <td>{{$key +1 }}</td>
                    <td>{{$item->sarpras->nama}}</td>
                    <td>{{$item->pengaju->name}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$key}}">
                            Lihat Laporan
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content"> 
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Laporan Terlampir</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                @php
                                    $detail = App\Models\PengajuanDetail::where('id_pengajuan',$item->id)->get();
                                @endphp
                                <div class="modal-body">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">Sarpras</th>
                                            <th scope="col">Pelapor</th>
                                            <th scope="col">Tingkat Kerusakan</th>
                                            <th scope="col">Keterangan</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detail as $value)
                                                <tr>
                                                    <td>{{$value->laporan->sarpras->nama}}</td>
                                                    <td>{{$value->laporan->pelapor->name}}</td>
                                                    <td>{{$value->laporan->tingkat_kerusakan}}</td>
                                                    <td>{{$value->laporan->keterangan}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </td>           
                    <td>{{$item->nominal}}</td>
                    @if ($item->status == 0)
                        <td><a href="/keuangan/pengajuansarpras/acc/{{$item->id}}" class="btn btn-success mx-1">ACC</a><a href="/keuangan/pengajuansarpras/reject/{{$item->id}}" class="btn btn-danger">Tolak</a></td>
                    @elseif ($item->status == 1)
                       <td>Sudah di ACC</td> 
                    @elseif ($item->status == 2)
                        <td>Ditolak</td>
                    @endif
                    @if ($item->status == 0)
                        <td>Belum di ACC</td>
                    @elseif ($item->status == 1)
                       <td>{{$item->penerima->name}}</td> 
                    @elseif ($item->status == 2)
                        <td>-</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection