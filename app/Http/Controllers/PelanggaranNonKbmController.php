<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Kelas;
use App\Models\PelaggaranNonKbm;
use App\Models\Semester;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PelanggaranNonKbmController extends Controller
{
    //
    public function index(){
        $auth = Auth::user();
        $kelas = Kelas::get();
        $pelanggaran = PelaggaranNonKbm::get();
        return view('bk.pelanggarannonkbm.index',compact('kelas', 'pelanggaran'));
    }

    public function form(Request $request){
        $auth = Auth::user();
        $kelas = Kelas::where('id',$request->id_kelas)->first();
        $siswa = Siswa::where('id_kelas',$kelas->id)->get();
        $guru = Guru::where('id_user',$auth->id)->first();
        return view('bk.pelanggarannonkbm.form',compact('kelas','siswa','guru'));
    }

    public function store(Request $request){
        // dd($request->input());
        $semester = Semester::where('is_aktif',1)->first();
        $pelanggaran = new PelaggaranNonKbm;
        $pelanggaran->id_siswa = $request->id_siswa;
        $pelanggaran->keterangan = $request->keterangan;
        $pelanggaran->id_semester = $semester->id;
        $pelanggaran->id_bk = $request->id_bk;
        $pelanggaran->tanggal_pelanggaran = $request->tanggal_pelanggaran;
        $pelanggaran->poin = $request->poin;
        $pelanggaran->save();
        
        return redirect('/bk/pelanggrarannonkbm');
    }
}
