<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Semester;
use App\Models\User;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index()
    {
        $kelas = Kelas::get();
        return view('kesiswaan.mapel.index',compact('kelas'));
    }

    public function indexkelas($id)
    {
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_kelas',$id)->where('id_semester',$semester->id)->get();
        return view('kesiswaan.mapel.indexkelas',compact('mapel','id'));
    }
    public function formmapel($id)
    {
        $kelas = Kelas::where('id',$id)->first();
        $guru = User::where('role','guru')->get();
        $semester = Semester::where('is_aktif',1)->first();
        return view('kesiswaan.mapel.form',compact('kelas','guru','semester'));
    }

    public function addmapel(Request $request)
    {
        
        $mapel = new Mapel;
        $mapel->nama = $request->nama;
        $mapel->id_guru = $request->id_guru;
        $mapel->id_kelas = $request->id_kelas;
        $mapel->id_semester = $request->id_semester;
        $mapel->save();

        return redirect('/kesiswaan/mapel/'.$request->id_kelas);
    }
}
