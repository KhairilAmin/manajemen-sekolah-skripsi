<?php

namespace App\Http\Controllers;

use App\Models\KomponenMapel;
use App\Models\Mapel;
use App\Models\NilaiSiswa;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GuruController extends Controller
{
    public function indexkomponen()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_guru',$auth->id)->where('id_semester',$semester->id)->get();
        return view('guru.komponen.index',compact('mapel'));
    }
    
    public function datakomponen($id)
    {
        $komponen = KomponenMapel::where('id_mapel',$id)->get();
        return view('guru.komponen.data',compact('komponen','id'));
    }

    public function formaddkomponen($id)
    {
        // dd($id);
        $mapel = Mapel::where('id',$id)->first();
        return view('guru.komponen.formadd',compact('mapel'));
    }
    
    public function actoinaddkomponen(Request $request)
    {
        // dd($request->input());
        $komponen = new KomponenMapel;
        $komponen->nama = $request->nama;
        $komponen->id_mapel = $request->id_mapel;
        $komponen->persentase = $request->persentase;
        $komponen->save();
        return redirect('/guru/komponennilai/'.$request->id_mapel);
    }

    public function indexinputnilai()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_guru',$auth->id)->where('id_semester',$semester->id)->get();
        return view('guru.nilai.index',compact('mapel'));
    }

    public function komponeninputnilai($id)
    {
        $mapel = Mapel::where('id',$id)->first();
        $komponen = KomponenMapel::where('id_mapel',$id)->get();
        return view('guru.nilai.komponen',compact('komponen','id','mapel'));
    }

    public function inputnilaisiswa($id)
    {
        $komponenmapel = KomponenMapel::where('id',$id)->first();
        $mapel = Mapel::where('id',$komponenmapel->id_mapel)->first();
        $siswa = Siswa::where('id_kelas', $mapel->kelas->id)->get();
        // dd($siswa);
        return view('guru.nilai.input',compact('mapel','siswa','komponenmapel'));
    }

    public function actioninputnilaisiswa(Request $request)
    {
        // dd($request->input());
        foreach ($request->id_siswa as $key=>$id_siswa){
            $cek = NilaiSiswa::where('id_siswa',$id_siswa)->where('id_komponen',$request->id_komponen)->first();
            if ($cek) {
                $nilai = $cek;
            } else {
                $nilai = new NilaiSiswa;
            }
            
            $nilai->id_komponen = $request->id_komponen;
            $nilai->id_siswa = $id_siswa;
            $nilai->nilai =  $request->nilai[$key];
            $nilai->save();
        }
        return redirect('/guru/inputnilai/');
    }
}
