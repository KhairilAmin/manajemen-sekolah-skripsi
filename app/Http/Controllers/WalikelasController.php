<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Kelas;
use App\Models\PelaggaranNonKbm;
use App\Models\Siswa;
use App\Models\Walikelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalikelasController extends Controller
{
    public function indexkesiswaan()
    {
        $kelas = Kelas::get();
        $guru = Guru::get();
        return view('kesiswaan.walikelas.index',compact('kelas','guru'));
    }

    public function setwalikelas(Request $request)
    {
        $cek = Walikelas::where('id_kelas',$request->id_kelas)->first();
        if ($cek == null) {
            $walikelas = new Walikelas;
            $walikelas->id_guru = $request->walikelas;
            $walikelas->id_kelas = $request->id_kelas;
            $walikelas->save();
        } else {
            $walikelas = Walikelas::where('id_kelas',$request->id_kelas)->first();
            $walikelas->id_guru = $request->walikelas;
            $walikelas->save();
        }
        
        return redirect('/kesiswaan/walikelas');
    }

    public function pelanggaranindex()
    {
        $auth = Auth::user();
        $guru = Guru::where('id_user',$auth->id)->first();
        $walikelas = Walikelas::where('id_guru',$guru->id)->first();
        $kelas = Kelas::where('id',$walikelas->id_kelas)->first();
        $siswa = Siswa::where('id_kelas',$kelas->id)->get();

        return view('guru.walikelas.indexpelanggaran',compact('siswa','kelas'));
    }

    public function tagihanindex()
    {
        $auth = Auth::user();
        $guru = Guru::where('id_user',$auth->id)->first();
        $walikelas = Walikelas::where('id_guru',$guru->id)->first();
        $kelas = Kelas::where('id',$walikelas->id_kelas)->first();
        $siswa = Siswa::where('id_kelas',$kelas->id)->get(); 

        return view('guru.walikelas.indextagihan', compact('kelas','siswa',));
    }
}
