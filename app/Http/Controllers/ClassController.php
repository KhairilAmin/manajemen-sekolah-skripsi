<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    public function indexkelas()
    {
        $kelas = Kelas::get();
        return view('kesiswaan.kelas.indexkelas',compact('kelas'));
    }

    public function indexaddkelas()
    {
        // $kelas = Kelas::get();
        return view('kesiswaan.kelas.addkelas');
    }

    public function addkelas(Request $request)
    {
        // dd($request->nama);
        $kelas = new Kelas;
        $kelas->nama = $request->nama;
        $kelas->tingkat = $request->tingkat;
        $kelas->save();
        return redirect('/kesiswaan/kelas');
    }
}
