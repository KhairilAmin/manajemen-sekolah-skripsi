<?php

namespace App\Http\Controllers;

use App\Imports\AbsensiImport;
use App\Models\AbsensiSiswa;
use App\Models\Kelas;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\TanggalAbsensi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AbsensiController extends Controller
{
    public function index()
    {
        return view('kesiswaan.absensi.index');
    }

    public function importabsensi(Request $request){
        $this->validate($request, [
            'excel_absensi'  => 'required|mimes:xls,xlsx'
        ]);
        $path = $request->file('excel_absensi')->getRealPath();
        // dd($path);
        // $data = Excel::load($path, function($reader) {})->get();
        $data = Excel::import(new AbsensiImport, $path);
        return back()->with('success', 'Excel Data Imported successfully.');
    }

    public function guruindex()
    {
        $semester = Semester::where('is_aktif',1)->first();
        $kelas = Kelas::get();
        return view('guru.absensi.index',compact('kelas','semester'));
    }

    public function guruindexaction(Request $request)
    {
        $kelas = Kelas::where('id',$request->id_kelas)->first();
        $tanggalabsensi = TanggalAbsensi::where('bulan',$request->bulan)->get();
        
        return view('guru.absensi.detail',compact('kelas','tanggalabsensi'));
    }

    public function isiabsensi($id_tanggal, $id_kelas)
    {
        $siswa = Siswa::where('id_kelas',$id_kelas)->get();
        $tanggal = TanggalAbsensi::where('id',$id_tanggal)->first();
        // dd($tanggal);
        return view('guru.absensi.input',compact('siswa','tanggal'));
    }

    public function submitabsensi(Request $request){
        // dd($request->input());
        $auth = Auth::user();
        foreach( $request->id_siswa as $key=>$id_siswa ){
            $absensi = AbsensiSiswa::where('id_siswa',$id_siswa)->where('id_tanggal_absensi',$request->tanggal)->first();
            $absensi->status = $request->status[$key];
            $absensi->id_guru = $auth->id;
            $absensi->save();
        }
        return redirect('/guru/absensi');
    }

    public function siswaindex()
    {
        $auth = Auth::user();
        return view('siswa.absensi.index',compact('auth'));
    }

    public function siswaindexdetail(Request $request)
    {
        // dd($request->input());
        $auth = Auth::user();
        $siswa = Siswa::where('id_user',$auth->id)->first();
        $bulan = $request->bulan;
        $tanggal = TanggalAbsensi::where('bulan',$request->bulan)->get();
        return view('siswa.absensi.selected',compact('auth','tanggal','bulan','siswa'));
    }
}
