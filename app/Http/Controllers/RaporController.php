<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\KomponenMapel;
use App\Models\LogKelas;
use App\Models\Mapel;
use App\Models\NilaiSiswa;
use App\Models\Rapor;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\Walimurid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RaporController extends Controller
{
    public function indexguru()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_guru',$auth->id)->where('id_semester',$semester->id)->get();
        return view('guru.generaterapor.index',compact('mapel'));
    }

    public function indexsiswa()
    {
        $auth = Auth::user();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $log = LogKelas::where('id_siswa',$siswa->id)->get();
        return view('siswa.rapor.index',compact('auth','siswa','log'));
    }

    public function indexsiswadetail(Request $request)
    {
        $auth = Auth::user();
        $log = LogKelas::where('id',$request->log)->first();
        $semester = Semester::where('id',$log->id_semester)->first();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $mapel = Mapel::where('id_kelas',$log->kelas->id)->where('id_semester',$semester->id)->get();

        return view('siswa.rapor.indexdetail',compact('mapel','auth','log','siswa'));
    }


    public function generateguru($id)
    {
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id',$id)->first();
        $siswas = Siswa::where('id_kelas',$mapel->id_kelas)->get();
        foreach ($siswas as $key=>$siswa)
        {
            $cek = Rapor::where('id_siswa',$siswa->id)->where('id_mapel',$mapel->id)->first();
            if ($cek) {
                $komponen = KomponenMapel::where('id_mapel',$mapel->id)->get();
                $nilai_angka = 0;
                foreach ($komponen as $key => $value) {
                    $nilai = NilaiSiswa::where('id_siswa',$siswa->id)->where('id_komponen',$value->id)->first();
                    $tempnilai = $nilai->nilai * $value->persentase / 100; 
                    $nilai_angka = $nilai_angka + $tempnilai;
                }
                if ($nilai_angka >= 90) {
                    $nilai_huruf = 'A';
                }elseif ($nilai_angka >= 80 && $nilai_angka < 90) {
                    $nilai_huruf = 'B';
                }elseif ($nilai_angka >= 70 && $nilai_angka < 80) {
                    $nilai_huruf = 'C';
                }elseif ($nilai_angka >= 60 && $nilai_angka < 70) {
                    $nilai_huruf = 'D';
                }elseif ($nilai_angka < 60) {
                    $nilai_huruf = 'E';
                }

                $rapor = Rapor::where('id_siswa',$siswa->id)->where('id_mapel',$mapel->id)->first();
                $rapor->nilai_angka = $nilai_angka;
                $rapor->nilai_huruf = $nilai_huruf;
                $rapor->save();
            } else {
                $komponen = KomponenMapel::where('id_mapel',$mapel->id)->get();
                $nilai_angka = 0;
                foreach ($komponen as $key => $value) {
                    $nilai = NilaiSiswa::where('id_siswa',$siswa->id)->where('id_komponen',$value->id)->first();
                    $tempnilai = $nilai->nilai * $value->persentase / 100; 
                    $nilai_angka = $nilai_angka + $tempnilai;
                }
                if ($nilai_angka >= 90) {
                    $nilai_huruf = 'A';
                }elseif ($nilai_angka >= 80 && $nilai_angka < 90) {
                    $nilai_huruf = 'B';
                }elseif ($nilai_angka >= 70 && $nilai_angka < 80) {
                    $nilai_huruf = 'C';
                }elseif ($nilai_angka >= 60 && $nilai_angka < 70) {
                    $nilai_huruf = 'D';
                }elseif ($nilai_angka < 60) {
                    $nilai_huruf = 'E';
                }

                $rapor = new Rapor;
                $rapor->id_siswa = $siswa->id;
                $rapor->id_semester = $semester->id;
                $rapor->id_mapel = $mapel->id;
                $rapor->nilai_angka = $nilai_angka;
                $rapor->nilai_huruf = $nilai_huruf;
                $rapor->save();
            }
        }
        return redirect('/guru/generaterapor');
    }

    public function seeguru()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_guru',$auth->id)->where('id_semester',$semester->id)->get();
        return view('guru.lihatrapor.index',compact('mapel'));
    }

    public function seedetailguru($id)
    {
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id',$id)->first();
        $siswas = Siswa::where('id_kelas',$mapel->id_kelas)->get();
        $kelas = Kelas::where('id',$mapel->id_kelas)->first();

        return view('guru.lihatrapor.detail',compact('mapel','siswas','mapel','kelas'));

    }
}
