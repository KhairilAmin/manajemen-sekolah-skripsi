<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use Illuminate\Http\Request;

class AdmisiController extends Controller
{
    public function indexlulus(){
        $kelas = Kelas::where('tingkat',6)->get();
        
        return view('kesiswaan.admisi.indexlulus',compact('kelas'));
    }

    public function detaillulus(Request $request){
        
        $id_kelas = $request->id_kelas;
        $kelas = Kelas::where('tingkat',6)->get();
        $siswa = Siswa::where('id_kelas',$id_kelas)->get();
        
        return view('kesiswaan.admisi.detaillulus', compact('kelas','siswa','id_kelas'));
    }

    public function setlulus(Request $request){
        foreach ($request->id as $siswas){
            $siswa = Siswa::where('id',$siswas)->first();
            $siswa->is_aktif = 0;
            $siswa->id_kelas = null;
            $siswa->save();
        }

        return redirect('/kesiswaan/lulus');
    }

    public function indexnaikkelas(){
        $kelas = Kelas::where('tingkat','!=',6)->get();
        
        return view('kesiswaan.admisi.indexnaikkelas',compact('kelas'));
    }

    public function detailnaikkelas(Request $request){
        
        $id_kelas = $request->id_kelas;
        $selectedkelas = Kelas::where('id',$id_kelas)->first();
        $kelas = Kelas::where('tingkat',($selectedkelas->tingkat + 1))->get();
        $siswa = Siswa::where('id_kelas',$id_kelas)->get();
        
        return view('kesiswaan.admisi.detailnaikkelas', compact('selectedkelas','kelas','siswa','id_kelas'));
    }

    public function setnaikkelas(Request $request){
        // dd($request->id);
        foreach ($request->id as $id){
            $siswa = Siswa::where('id',$id)->first();
            $siswa->id_kelas = $request->id_kelas;
            $siswa->save();
        }
        return redirect('/kesiswaan/naikkelas');

    }
}
