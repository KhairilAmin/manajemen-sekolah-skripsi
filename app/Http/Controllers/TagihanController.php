<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Tagihan;
use App\Models\Walimurid;
use Illuminate\Http\Request;

class TagihanController extends Controller
{
    public function indextagihan()
    {
        $kelas = Kelas::get();
        return view('keuangan.generatetagihan.generatetagihan',compact('kelas'));
    }

    public function formgenerate($id_kelas)
    {
        // dd($id_kelas);
        $kelas = Kelas::where('id',$id_kelas)->first();
        return view('keuangan.generatetagihan.formtagihan',compact('kelas'));
    }

    public function saveformgenerate(Request $request)
    {
        $kelas = Kelas::where('id',$request->kelas)->first();
        $siswa = Siswa::where('id_kelas',$kelas->id)->get();
        // dd($request->input());
        foreach ( $siswa as $s ){
            $tagihan = new Tagihan;
            $tagihan->siswa_id = $s->id;
            $tagihan->is_tagih = 1;
            $tagihan->nominal = $request->nominal;
            $tagihan->keterangan = $request->keterangan;
            $tagihan->offdate = $request->offdate;
            $tagihan->save();
        }
        return redirect('/keuangan/generatespp');
    }

    public function pembayaransiswa()
    {
        $auth = auth()->user()->id;
        if (auth()->user()->role == 'siswa'){
            $siswa = Siswa::where('id_user',$auth)->first();
        }elseif(auth()->user()->role == 'walimurid'){
            $walimurid = Walimurid::where('id_user',$auth)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $pembayaranselesai = Tagihan::where('siswa_id',$siswa->id)->where('is_tagih',0)->get();
        $pembayaranbelum = Tagihan::where('siswa_id',$siswa->id)->where('is_tagih',1)->get();
        // dd($pembayaranselesai);
        if (auth()->user()->role == 'siswa'){
            return view('siswa.pembayaran.datapembayaran',compact('pembayaranbelum','pembayaranselesai'));
        }elseif(auth()->user()->role == 'walimurid'){
            return view('walimurid.pembayaran.datapembayaran',compact('pembayaranbelum','pembayaranselesai','siswa'));
        }
    }
    
    public function indexdatatagihan()
    {
        $kelas = Kelas::get();
        return view('keuangan.datatagihan.indexdatatagihan',compact('kelas'));
    }

    public function selectedclass(Request $request)
    {
        // dd($request->input());
        return redirect('/keuangan/datatagihan/selectedclass/'.$request->id_kelas);

    }

    public function tagihansiswakelas($id_kelas)
    {
        $kelas = Kelas::get();
        $siswa = Siswa::where('id_kelas',$id_kelas)->get(); 

        return view('keuangan.datatagihan.tagihankelas', compact('kelas','siswa','id_kelas'));
    }

    public function terimapembayaran(Request $request)
    {
        foreach ($request->id as $id){
            $tagihan = Tagihan::where('id',$id)->first();
            $tagihan->is_tagih = 0;
            $tagihan->id_penerima = auth()->user()->id;
            $tagihan->save();
        }
        return redirect('/keuangan/datatagihan');

    }
}
