<?php

namespace App\Http\Controllers;

use App\Models\KomponenMapel;
use App\Models\LogKelas;
use App\Models\Mapel;
use App\Models\NilaiSiswa;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\Walimurid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NilaiSiswaController extends Controller
{
    public function index()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $mapel = Mapel::where('id_kelas',$siswa->kelas->id)->where('id_semester',$semester->id)->get();

        return view('siswa.nilai.index',compact('mapel','auth'));
    }

    public function indexnilai($id)
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id',$id)->where('id_semester',$semester->id)->first();
        $komponen = KomponenMapel::where('id_mapel',$mapel->id)->get();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        
        return view('siswa.nilai.data',compact('mapel','siswa','auth','komponen'));
    }

    public function indexbefore(){
        $auth = Auth::user();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $log = LogKelas::where('id_siswa',$siswa->id)->get();
        return view('siswa.before.index',compact('auth','siswa','log'));
    }

    public function indexbeforemapel(Request $request)
    {
        $auth = Auth::user();
        $log = LogKelas::where('id',$request->log)->first();
        $semester = Semester::where('id',$log->id_semester)->first();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        $mapel = Mapel::where('id_kelas',$log->kelas->id)->where('id_semester',$semester->id)->get();

        return view('siswa.before.indexmapel',compact('mapel','auth','log'));
    }

    public function indexbeforenilai($id_mapel,$id_log)
    {
        $auth = Auth::user();
        $log = LogKelas::where('id',$id_log)->first();
        $semester = Semester::where('id',$log->id_semester)->first();
        $mapel = Mapel::where('id',$id_mapel)->where('id_semester',$semester->id)->first();
        $komponen = KomponenMapel::where('id_mapel',$mapel->id)->get();
        if($auth->role = 'siswa'){
            $siswa = Siswa::where('id_user',$auth->id)->first();
        }else{
            $walimurid = Walimurid::where('id_user',$auth->id)->first();
            $siswa = Siswa::where('id',$walimurid->id_siswa)->first();
        }
        
        return view('siswa.before.data',compact('mapel','siswa','auth','komponen','log'));
    }

}
