<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboardsiswa()
    {
        return view('siswa.dashboard');
    }

    public function dashboardwalimurid()
    {
        return view('walimurid.dashboard');
    }

    public function dashboardguru()
    {
        return view('guru.dashboard');
    }
    
    public function dashboardkesiswaan()
    {
        return view('kesiswaan.dashboard');
    }

    public function dashboardbk()
    {
        return view('bk.dashboard');
    }

    public function dashboardkeuangan()
    {
        return view('keuangan.dashboard');
    }

    public function dashboardsarpras()
    {
        return view('sarpras.dashboard');
    }
}
