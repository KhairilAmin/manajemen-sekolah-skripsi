<?php

namespace App\Http\Controllers;

use App\Models\LaporanSarpras;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaporanSarprasController extends Controller
{
    public function indexdatalaporansarpras()
    {
        $laporan = LaporanSarpras::get();
        return view('sarpras.laporansarpras.index',compact('laporan'));
    }

    public function indexcreatelaporan()
    {
        $user = Auth::user();
        $laporan = LaporanSarpras::where('id_pelapor',$user->id)->get();
        return view('laporansarpras.indexadd',compact('laporan','user'));
    }

    public function formlaporan()
    {
        $user = Auth::user();
        $sarpras = Sarpras::get();
        return view('laporansarpras.form',compact('user','sarpras'));
    }

    public function addlaporan(Request $request)
    {
        // dd($request->input());
        $auth = Auth::user();
        
        $laporan = new LaporanSarpras;
        $laporan->id_sarpras = $request->id_sarpras;
        $laporan->id_pelapor = $auth->id;
        $laporan->tingkat_kerusakan = $request->tingkat_kerusakan;
        $laporan->keterangan = $request->keterangan;
        $laporan->status = 0;
        $laporan->save();

        return redirect('/'.$auth->role .'/laporansarpras');
    }

}
