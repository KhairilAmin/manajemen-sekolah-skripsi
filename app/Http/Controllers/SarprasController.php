<?php

namespace App\Http\Controllers;

use App\Models\Sarpras;
use Illuminate\Http\Request;

class SarprasController extends Controller
{
    public function indexdatasarpras()
    {
        $sarpras = Sarpras::get();
        return view('sarpras.datasarpras.index',compact('sarpras'));
    }
    
    public function indexadddatasarpras()
    {
        return view('sarpras.datasarpras.indexadd');
    }
    public function adddatasarpras(Request $request)
    {
        // dd($request->input());
        $sarpras = new Sarpras;
        $sarpras->nama = $request->nama;
        $sarpras->jumlah = $request->jumlah;
        $sarpras->save();
        return redirect('/sarpras/datasarpras');
    }
}
