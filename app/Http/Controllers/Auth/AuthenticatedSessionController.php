<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        $request->session()->regenerate();
        if(Auth::user() && Auth::user()->role == 'guru'){
            return redirect()->route('dashboard-guru');
        } elseif (Auth::user() && Auth::user()->role == 'siswa') {
            return redirect()->route('dashboard-siswa');
        } elseif (Auth::user() && Auth::user()->role == 'walimurid') {
            return redirect()->route('dashboard-walimurid');
        } elseif (Auth::user() && Auth::user()->role == 'kesiswaan') {
            return redirect()->route('dashboard-kesiswaan');
        } elseif (Auth::user() && Auth::user()->role == 'bk') {
            return redirect()->route('dashboard-bk');
        } elseif (Auth::user() && Auth::user()->role == 'keuangan') {
            return redirect()->route('dashboard-keuangan');
        } elseif (Auth::user() && Auth::user()->role == 'sarpras') {
            return redirect()->route('dashboard-sarpras');
        } else { 
            Auth::guard('web')->logout();
            return redirect()->route('login')->with('status', 'You are not authorized to access this page.');
        }
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
