<?php

namespace App\Http\Controllers;

use App\Imports\SiswaImport;
use App\Models\Guru;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use App\Models\Walimurid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class AccountController extends Controller
{
    //Siswa
    public function indexakunsiswa()
    {
        $kelas = Kelas::get();
        return view('kesiswaan.akunsiswa.indexakunsiswa', compact('kelas'));
    }
    public function indexaddakunsiswa()
    {
        $kelas = Kelas::get();
        return view('kesiswaan.akunsiswa.indexaddakunsiswa',compact('kelas'));
    }

    public function addakunsiswa(Request $request)
    {
        // dd($request->input());
        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->role = 'siswa';
        $user->password = Hash::make($request->password);
        $user->save();
        
        $siswa = new Siswa;
        $siswa->nama = $request->nama;
        $siswa->id_user = $user->id;
        $siswa->id_kelas = $request->id_kelas;
        $siswa->alamat = $request->alamat;
        $siswa->nis_siswa = $request->nis_siswa;
        $siswa->save();

        $user1 = new User;
        $user1->name = $request->namawali;
        $user1->email = $request->nohp . '@sditalhidayahsumenep.wali.ac.id';
        $user1->role = 'walimurid';
        $user1->password = Hash::make($request->password);
        $user1->save();

        $wali = new Walimurid;
        $wali->nama = $request->namawali;
        $wali->id_user = $user1->id;
        $wali->id_siswa = $siswa->id;
        $wali->no_hp = $request->nohp;
        $wali->save();

        return redirect('/kesiswaan/akunsiswa');
    }
    
    public function selectedclass(Request $request)
    {
        // dd($request->input());
        return redirect('/kesiswaan/akunsiswa/selectedclass/'.$request->id_kelas);

    }

    public function indexakunsiswakelas($id_kelas)
    {
        $kelas = Kelas::get();
        $siswa = Siswa::where('id_kelas',$id_kelas)->get();
        // dd($siswa->kelas);
        return view('kesiswaan.akunsiswa.selectedclass', compact('kelas','siswa','id_kelas'));
    }

    //Guru
    public function indexakunguru()
    {
        $guru = Guru::get();
        return view('kesiswaan.akunguru.indexakunguru', compact('guru'));
    }

    public function indexaddakunguru()
    {
        return view('kesiswaan.akunguru.indexaddakunguru');
    }

    public function addakunguru(Request $request)
    {
        // dd($request->input());
        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = Hash::make($request->password);
        $user->save();

        $guru = new Guru;
        $guru->nama = $request->nama;
        $guru->id_user = $user->id;
        $guru->nip = $request->nip;
        $guru->alamat = $request->alamat;
        $guru->save();

        return redirect('/kesiswaan/akunguru');
    }

    public function importsiswa(Request $request){
        $this->validate($request, [
            'excel_siswa'  => 'required|mimes:xls,xlsx'
        ]);
        $path = $request->file('excel_siswa')->getRealPath();
        // dd($path);
        // $data = Excel::load($path, function($reader) {})->get();
        $data = Excel::import(new SiswaImport, $path);
        return back()->with('success', 'Excel Data Imported successfully.');
    }
}
