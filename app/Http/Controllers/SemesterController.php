<?php

namespace App\Http\Controllers;

use App\Models\LogKelas;
use App\Models\Semester;
use App\Models\Siswa;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
    public function index()
    {
        $semester = Semester::get();
        return view('kesiswaan.semester.index',compact('semester'));
    }

    public function indexadd()
    {
        // $semester = Semester::get();
        return view('kesiswaan.semester.indexadd');
    }

    public function actionadd(Request $request)
    {
        // dd($request->input());
        $semester = new Semester;
        $semester->nama = $request->nama;
        $semester->is_aktif = 0;
        $semester->save();

        return redirect('/kesiswaan/semester');
    }

    public function setaktif(Request $request)
    {
        // dd($request->input());
        $sem = Semester::where('is_aktif',1)->first();
        $sem->is_aktif = 0;
        $sem->save();

        $semester = Semester::where('id',$request->id_semester)->first();
        $semester->is_aktif = 1;
        $semester->save();

        return redirect('/kesiswaan/semester');
    }
    
    public function generatelogkelas(Request $request)
    {
        $siswa = Siswa::where('id_kelas','!=',null)->get();
        $sem = Semester::where('is_aktif',1)->first();
        
        foreach ($siswa as $s) { 
            $log = new LogKelas;
            $log->id_siswa = $s->id;
            $log->id_kelas = $s->kelas->id;
            $log->id_semester = $sem->id;
            $log->save();
        };
        
        return redirect('/kesiswaan/semester');
    }
}
