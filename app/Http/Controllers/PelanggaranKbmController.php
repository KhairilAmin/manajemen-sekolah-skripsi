<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Mapel;
use App\Models\PelanggaranKbm;
use App\Models\Semester;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PelanggaranKbmController extends Controller
{
    public function guruindex()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $mapel = Mapel::where('id_guru',$auth->id)->where('id_semester',$semester->id)->get();
        return view('guru.pelanggarankbm.index',compact('mapel'));
    }

    public function forminput($id)
    {
        $auth = Auth::user();
        $mapel = Mapel::where('id',$id)->first();
        $pelanggaran = PelanggaranKbm::where('id_mapel',$id)->get();
        $siswa = Siswa::where('id_kelas',$mapel->kelas->id)->get();
        $pelanggaran = PelanggaranKbm::where('id_mapel',$mapel->id)->get();
        // dd($siswa);
        return view('guru.pelanggarankbm.form',compact('mapel','siswa','pelanggaran','pelanggaran'));
    }

    public function addforminput(Request $request)
    {
        $auth = Auth::user();
        $guru = Guru::where('id_user',$auth->id)->first();
        $semester = Semester::where('is_aktif',1)->first();
        $pelanggaran = new PelanggaranKbm;
        $pelanggaran->id_siswa = $request->id_siswa;
        $pelanggaran->id_guru = $guru->id;
        $pelanggaran->id_semester = $semester->id;
        $pelanggaran->keterangan = $request->keterangan;
        $pelanggaran->id_mapel = $request->id_mapel;
        $pelanggaran->tanggal_pelanggaran = $request->tanggal_pelanggaran;
        $pelanggaran->save();

        return redirect('/guru/pelanggarankbm/'.$request->id_mapel);
    }

    public function managepelanggranbk()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $pelanggaranbelum = PelanggaranKbm::where('id_semester',1)->whereNull('id_bk')->get();
        $pelanggaranselesai = PelanggaranKbm::where('id_semester',1)->whereNotNull('id_bk')->get();
        // dd($pelanggaranselesai);

        return view('bk.pelanggarankbm.index',compact('pelanggaranbelum','pelanggaranselesai'));
    }
    
    public function actionpelanggranbk(Request $request)
    {
        $auth = Auth::user();
        $bk = Guru::where('id_user',$auth->id)->first();
        $pelanggaran = PelanggaranKbm::where('id',$request->id)->first();
        $pelanggaran->poin = $request->poin;
        $pelanggaran->id_bk = $bk->id;
        $pelanggaran->save();

        return redirect('bk/pelanggarankbm');

    }

    public function siswaindex()
    {
        $auth = Auth::user();
        $semester = Semester::where('is_aktif',1)->first();
        $siswa = Siswa::where('id_user',$auth->id)->first();
        $pelanggaranbelum = PelanggaranKbm::where('id_semester',1)->whereNull('id_bk')->where('id_siswa',$siswa->id)->get();
        $pelanggaranselesai = PelanggaranKbm::where('id_semester',1)->whereNotNull('id_bk')->where('id_siswa',$siswa->id)->get();
        
        return view('siswa.pelanggarankbm.index',compact('pelanggaranbelum','pelanggaranselesai','auth'));
    }
}
