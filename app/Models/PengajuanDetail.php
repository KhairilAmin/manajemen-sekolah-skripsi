<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PengajuanSarpras;
use App\Models\LaporanSarpras;

class PengajuanDetail extends Model
{
    protected $table = "pengajuan_detail";

    protected $fillable = ["id_pengajuan","id_laporan"];
    
    use HasFactory;

    public function pengajuan()
    {
        return $this->belongsTo(PengajuanSarpras::class ,'id_pengajuan');
    }

    public function laporan()
    {
        return $this->belongsTo(LaporanSarpras::class ,'id_laporan');
    }
}
