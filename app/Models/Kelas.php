<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mapel;
use App\Models\LogKelas;
class Kelas extends Model
{
    protected $table = "kelas";

    protected $fillable = ["nama","tingkat"];
    
    use HasFactory;

    public function siswa()
    {
        return $this->hasMany(Siswa::class);
    }

    public function mapel()
    {
        return $this->hasMany(Mapel::class);
    }

    public function logkelas()
    {
        return $this->hasMany(LogKelas::class);
    }

    public function walikelas()
    {
        return $this->hasOne(Walikelas::class);
    }
}
