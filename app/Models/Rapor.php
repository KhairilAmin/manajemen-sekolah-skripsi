<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rapor extends Model
{
    protected $table = "rapor";

    protected $fillable = ["id_siswa", "id_semester", "id_mapel", "nilai_angka","nilai_huruf"];
    
    use HasFactory;

    public function mapel()
    {
        return $this->belongsTo(Mapel::class ,'id_mapel');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class ,'id_semester');
    }
}
