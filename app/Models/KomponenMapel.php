<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mapel;

class KomponenMapel extends Model
{
    protected $table = "komponen_mapel";

    protected $fillable = ["nama", "id_mapel", "persentase"];
    
    use HasFactory;

    public function mapel()
    {
        return $this->belongsTo(Mapel::class ,'id_mapel');
    }

    public function nilai()
    {
        return $this->hasMany(NilaiSiswa::class);
    }
}
