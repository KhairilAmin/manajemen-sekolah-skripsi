<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;

class NilaiSiswa extends Model
{
    protected $table = "nilai_siswa";

    protected $fillable = ["id_komponen", "id_siswa", "nilai"];
    
    use HasFactory;

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }

    public function komponen()
    {
        return $this->belongsTo(KomponenMapel::class ,'id_komponen');
    }
}
