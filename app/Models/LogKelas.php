<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\Kelas;

class LogKelas extends Model
{
    protected $table = "log_kelas";

    protected $fillable = ["id_siswa","id_kelas","id_semester"];

    use HasFactory;

    public function semester()
    {
        return $this->belongsTo(Semester::class ,'id_semester');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class ,'id_kelas');
    }
}
