<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsensiSiswa extends Model
{
    protected $table = "absensi_siswa";

    protected $fillable = ["id_siswa","id_tanggal_absensi", "id_guru", "status", "keterangan"];
    
    use HasFactory;
}
