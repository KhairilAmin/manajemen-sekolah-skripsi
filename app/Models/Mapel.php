<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Semester;
use App\Models\User;
use App\Models\Kelas;
use App\Models\KomponenMapel;
use App\Models\PelanggaranKbm;

class Mapel extends Model
{
    protected $table = "mapel";

    protected $fillable = ["nama", "id_guru", "id_kelas","id_semester"];
    
    use HasFactory;

    public function semester()
    {
        return $this->belongsTo(Semester::class ,'id_semester');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class ,'id_kelas');
    }

    public function pengajar()
    {
        return $this->belongsTo(User::class ,'id_guru');
    }

    public function komponen()
    {
        return $this->hasMany(KomponenMapel::class);
    }

    public function pelanggarankbm()
    {
        return $this->hasMany(PelanggaranKbm::class);
    }

    public function rapor()
    {
        return $this->hasMany(Rapor::class);
    }

}
