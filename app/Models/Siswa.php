<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Tagihan;
use App\Models\LogKelas;

class Siswa extends Model
{
    protected $table = "siswa";

    protected $fillable = ["id_user","nama", "id_kelas", "alamat","nis_siswa"];
    
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class ,'id_user');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class ,'id_kelas');
    }

    public function tagihan()
    {
        return $this->hasMany(Tagihan::class);
    }

    public function nilaikomponen()
    {
        return $this->hasMany(NilaiSiswa::class);
    }
    
    public function logkelas()
    {
        return $this->hasMany(LogKelas::class);
    }

    public function pelangarankbm()
    {
        return $this->hasMany(PelanggaranKbm::class);
    }

    public function pelangarannonkbm()
    {
        return $this->hasMany(PelaggaranNonKbm::class);
    }

    public function rapor()
    {
        return $this->hasMany(Rapor ::class);
    }
}
