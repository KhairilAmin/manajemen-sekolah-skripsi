<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Guru extends Model
{
    protected $table = "guru";

    protected $fillable = ["id_user","nama", "nip", "alamat"];
    
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class ,'id_user');
    }

    public function pelanggarankbm()
    {
        return $this->hasMany(PelanggaranKbm::class);
    }

    public function pelanggarannonkbm()
    {
        return $this->hasMany(PelaggaranNonKbm::class);
    }

    public function walikelas()
    {
        return $this->hasOne(Walikelas::class);
    }
    
}
