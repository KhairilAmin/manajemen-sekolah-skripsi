<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;
use App\Models\User;

class Tagihan extends Model
{
    protected $table = "tagihan";

    protected $fillable = ["siswa_id","is_tagih", "nominal", "keterangan", "offdate", "id_penerima"];
    
    use HasFactory;

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'siswa_id');
    }
    public function penerima()
    {
        return $this->belongsTo(User::class ,'id_penerima');
    }
}
