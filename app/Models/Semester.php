<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $table = "semester";

    protected $fillable = ["nama","is_aktif"];

    use HasFactory;

    public function mapel()
    {
        return $this->hasMany(Mapel::class);
    }

    public function pelanggarankbm()
    {
        return $this->hasMany(PelanggaranKbm::class);
    }

    public function pelanggarannonkbm()
    {
        return $this->hasMany(PelanggaranKbm::class);
    }
    
    public function logkelas()
    {
        return $this->hasMany(LogKelas::class);
    }

}
