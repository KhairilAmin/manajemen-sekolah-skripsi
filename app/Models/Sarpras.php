<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\LaporanSarpras;
use App\Models\PengajuanSarpras;

class Sarpras extends Model
{
    protected $table = "sarpras";

    protected $fillable = ["nama","jumlah"];

    use HasFactory;

    public function laporansapras()
    {
        return $this->hasMany(LaporanSarpras::class);
    }

    public function pengajuan()
    {
        return $this->hasMany(PengajuanSarpras::class);
    }
}
