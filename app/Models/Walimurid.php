<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;
use App\Models\User;

class Walimurid extends Model
{
    protected $table = "walimurid";

    protected $fillable = ["id_user","nama", "id_siswa", "no_hp",];
    
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class ,'id_user');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }
}
