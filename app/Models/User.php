<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Siswa;
use App\Models\Guru;
use App\Models\Walimurid;
use App\Models\Tagihan;
use App\Models\LaporanSarpras;
use App\Models\PengajuanSarpras;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function siswa()
    {
        return $this->hasOne(Siswa::class);
    }

    public function guru()
    {
        return $this->hasOne(Guru::class);
    }

    public function walimurid()
    {
        return $this->hasOne(Walimurid::class);
    }

    public function penerimatagihan()
    {
        return $this->hasMany(Tagihan::class);
    }

    public function LaporanSarpras()
    {
        return $this->hasMany(LaporanSarpras::class);
    }

    public function pengajuan_sarpras()
    {
        return $this->hasMany(PengajuanSarpras::class);
    }
    public function penerima_pengajuan_sarpras()
    {
        return $this->hasMany(PengajuanSarpras::class);
    }

    public function pengajar()
    {
        return $this->hasMany(Mapel::class);
    }

}

