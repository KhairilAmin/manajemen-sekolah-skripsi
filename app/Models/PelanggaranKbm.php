<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelanggaranKbm extends Model
{
    protected $table = "pelanggaran_kbm";

    protected $fillable = ["id_siswa", "id_guru", "id_semester", "id_mapel", "id_bk","keterangan", "tanggal_pelanggaran", "poin"];
    
    use HasFactory;

    public function mapel()
    {
        return $this->belongsTo(Mapel::class ,'id_mapel');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class ,'id_semester');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }

    public function guru()
    {
        return $this->belongsTo(Guru::class ,'id_guru');
    }

    public function bk()
    {
        return $this->belongsTo(Guru::class ,'id_bk');
    }
}
