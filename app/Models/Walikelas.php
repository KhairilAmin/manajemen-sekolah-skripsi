<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Walikelas extends Model
{
    protected $table = "walikelas";

    protected $fillable = ["id_guru","id_kelas"];
    
    use HasFactory;

    public function kelas()
    {
        return $this->belongsTo(Kelas::class ,'id_kelas');
    }

    public function guru()
    {
        return $this->belongsTo(Guru::class ,'id_guru');
    }
}
