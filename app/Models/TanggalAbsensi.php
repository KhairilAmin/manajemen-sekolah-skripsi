<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TanggalAbsensi extends Model
{
    protected $table = "tanggal_absensi";

    protected $fillable = ["tanggal","statushari", "hari", "bulan", "id_semester"];
    
    use HasFactory;
}
