<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelaggaranNonKbm extends Model
{
    protected $table = "pelanggaran_nonkbm";

    protected $fillable = ["id_siswa", "id_bk", "id_semester", "keterangan", "tanggal_pelanggaran", "poin"];
    
    use HasFactory;

    public function semester()
    {
        return $this->belongsTo(Semester::class ,'id_semester');
    }

    public function siswa()
    {
        return $this->belongsTo(Siswa::class ,'id_siswa');
    }

    public function bk()
    {
        return $this->belongsTo(Guru::class ,'id_bk');
    }

}
