<?php

namespace App\Imports;

use App\Models\AbsensiSiswa;
use App\Models\Semester;
use App\Models\Siswa;
use App\Models\TanggalAbsensi;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AbsensiImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $tanggalabsensi = TanggalAbsensi::create([
                'tanggal' => $row[0],
                'statushari'    => $row[2],
                'hari' => $row[1],
                'id_semester' => Semester::where('is_aktif',1)->first()->id,
                'bulan' => $row[3],
                'keterangan' => $row[4],
            ]);
            $siswas = Siswa::get();
            foreach ($siswas as $siswa){
                $absensisiswa = AbsensiSiswa::create([
                    'id_siswa' => $siswa->id,
                    'id_tanggal_absensi' => $tanggalabsensi->id,
                    'status' => 0,
                ]);
            }
      }
    }
}
