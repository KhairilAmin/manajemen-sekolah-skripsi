<?php

namespace App\Imports;

use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use App\Models\Walimurid;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;

class SiswaImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $user = User::create([
                'name' => $row[1],
                'email'    => $row[2],
                'role' => 'siswa',
                'password' => Hash::make($row[3]),
            ]);
            $kelas = Kelas::where('nama',$row[4])->first();
            $siswa = Siswa::create([
                    'nama' => $row[1],
                    'id_user' => $user->id,
                    'id_kelas' => $kelas->id,
                    'alamat' => $row[5],
                    'nis_siswa' => $row[0],
            ]);
            $user1 = User::create([
                    'name' => $row[6],
                    'email'    => $row[7] . '@sditalhidayahsumenep.wali.ac.id',
                    'role' => 'walimurid',
                    'password' => Hash::make('12345678'),
            ]);
            $walimurid = Walimurid::create([
                    'nama' => $row[6],
                    'id_user' => $user1->id,
                    'id_siswa' => $siswa->id,
                    'no_hp' => $row[7],
            ]);
      }
   }
}
